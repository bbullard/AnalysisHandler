#ifndef MT_SAMPLE_HANDLER_H
#define MT_SAMPLE_HANDLER_H

#include <iostream>
#include <string>
#include <thread>
#include <utility>
#include <vector>
#include <fstream>
#include <TSystem.h>
#include <TString.h>
#include "LoadingBar.h"

using namespace std;

namespace AH {

  template < class DerivedSH >
  class MTSampleHandler {

    public:
      MTSampleHandler< DerivedSH >( Int_t nThreads = 0 );
      ~MTSampleHandler();
      
      void Initialize( string ntuple_name, string path_to_files, string label );
      DerivedSH* Process( Bool_t test = false );

    private:
      vector< DerivedSH* > sample_handlers;
      Int_t nThreads;
      string sample_label;
      string ntuple_name;
      string label;
  };

  template < class DerivedSH >
  MTSampleHandler<DerivedSH>::MTSampleHandler( Int_t nThreads ) {
    
    if ( nThreads == 0 )
      nThreads = std::thread::hardware_concurrency() - 2;
    this->nThreads = nThreads;
  }

  template < class DerivedSH >
  MTSampleHandler<DerivedSH>::~MTSampleHandler() {
    
    for ( auto* SH : sample_handlers )
      delete SH;
  }

  vector< string > GetOrderedFiles( string path_to_datasets ) {
    
    vector< string > orderedFiles;
    ifstream inFile( path_to_datasets.c_str() );
    string data_file;
    while ( getline( inFile, data_file ) )
      orderedFiles.push_back( data_file );
    
    return orderedFiles;
  }

  template < class DerivedSH >
  void MTSampleHandler<DerivedSH>::Initialize( string ntuple_name, string path_to_files, string label ) {

    this->ntuple_name = ntuple_name;
    this->label = label;

    vector< string > ordered_files = GetOrderedFiles( path_to_files );
    if ( (Int_t)ordered_files.size() < nThreads ) {
      cerr << "Warning:\tSetting the number of threads from " << nThreads << " to " << ordered_files.size() << ", the number of input files." << endl;
      nThreads = ordered_files.size();
    }
    
    for ( Int_t i = 0; i < nThreads; i++ ) {
      sample_handlers.push_back( new DerivedSH( ntuple_name, "", true ) );
      sample_handlers.at(i)->Initialize( label + "_" + to_string(i) ); 
    }
    
    Int_t i_handler = 0;
    for ( auto path : ordered_files )
      sample_handlers.at( (i_handler++)%nThreads )->AddFile( path );
  }

  template < class DerivedSH >
  DerivedSH* MTSampleHandler<DerivedSH>::Process( Bool_t test ) {

    UInt_t nEvents_threads = 0; 
    for ( Int_t i = 0; i < nThreads; i++ )
      nEvents_threads += sample_handlers.at(i)->GetNEvents();

    LoadingBar b( nEvents_threads );

    DerivedSH* combined_SH = new DerivedSH( ntuple_name, "", true );
    combined_SH->Initialize( label );

    cout << "Info:\tMTSampleHandler creating " << nThreads << " threads to process ISampleHandler " << combined_SH->GetName() << endl;
    vector< thread > threads = {};
    for ( Int_t i = 0; i < nThreads; i++ ) {
      threads.push_back( thread( &DerivedSH::Process, sample_handlers.at(i), test ) );
    }
    
    cout << "Info:\tProgress toward thread completion:" << endl;
    b.Increment( 0 );
    for ( Int_t i = 0; i < nThreads; i++ ) {
      threads.at(i).join();
      combined_SH->Add( sample_handlers.at(i) );
      b.Increment( sample_handlers.at(i)->GetNEvents() );
    }
    b.Finish();
    
    if ( nEvents_threads != combined_SH->GetNEvents() )
      cerr << "Warning:\t Number of events in returned ISampleHandler is " << combined_SH->GetNEvents() << " but should be " << nEvents_threads << " from threads." << endl;
 
    return combined_SH;
  }
}

#endif
