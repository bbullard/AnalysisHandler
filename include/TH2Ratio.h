#ifndef TH2_RATIO_H
#define TH2_RATIO_H

#include <string>
#include <tuple>
#include "TPad.h"
#include "TCanvas.h"
#include "TH2.h"
#include "TLegend.h"
#include "THStack.h"
#include "Utility.h"
#include "FormatLibrary.h"

using namespace std;

namespace AH {
  
  class TH2Ratio {	
  
    enum Type { ratio, difference, percent_difference };

    /*
     * TH2Ratio class declaration
     */
    public:
      TH2Ratio( Type t = Type::ratio );
      ~TH2Ratio();
      
      Int_t AddNumerator( TH2 *h2, string label );
      Int_t AddDenominator( TH2 *h2, string label );
      void ResetNumerator();
      void ResetDenominator(); 

      void SetScaleToNumerator( Bool_t option = true ) { scale_to_numerator = option; }
      void SetShapeAnalysis( Bool_t option = true ) { shape_analysis = option; }
      
      Int_t Draw( TCanvas *can  );
      
      TH1*  GetComparisonDistribution() { return h1_ratio; }
      Int_t GetNBins();
  
    private:
      void  MakeComparisonDistribution();
      
      Bool_t HistogramsAreReady();
      Bool_t IsGoodBinning( TH1* h1 );

      TH1* h1_numerator;
      string numerator_legend_label;
      string numerator_axis_label;
      vector< TH1* > h1_denominator;
      vector< string > denominator_legend_label;
      string denominator_axis_label;
      
      TH1 *h1_ratio;
      Type plot_type;

      string x_label;	
      Int_t line_option;
      Bool_t shape_analysis;
      Bool_t scale_to_numerator;
  };

  /*
   * Constructor. Initialize meta-data
   */
  TH2Ratio::RatioPlot( Type t ) {  
    plot_type = t;
    h1_numerator = NULL;
    h1_ratio = NULL;
    denominator_legend_label = vector< string >(0);
    numerator_legend_label = "";
    denominator_axis_label = "";
    numerator_axis_label = "";
    line_option = 0;

    shape_analysis = false;
    scale_to_numerator = false;
  }

  /*
   * Destructor. Delete ratio plot
   */
  TH2Ratio::~RatioPlot() {
    
    delete h1_ratio; h1_ratio = NULL;
  }

  /*
   * Set histogram in numerator, check for overwrite
   */
  Int_t TH2Ratio::AddNumerator( TH1 *h1, string label ) {

    if ( h1_numerator ) {
      cerr << "Warning:\tCannot set histogram " << h1->GetName() << " to numerator plot - already set." << endl;
      return -1;
    }

    if ( !IsGoodBinning( h1 ) ) {
      cerr << "Warning:\tCannot use histogram " << h1->GetName() << " - inconsistent binning with denominator" << endl;
      return -2;
    }

    h1_numerator = h1;
    numerator_legend_label = label;

    return 0;
  }

  /*
   * Add histogram in the denominator
   */
  Int_t TH2Ratio::AddDenominator( TH1 *h1, string label ) {

    if ( !IsGoodBinning( h1 ) ) {
      cerr << "Warning:\tCannot use histogram " << h1->GetName() << " - inconsistent binning" << endl;
      return -1;
    }

    h1_denominator.push_back( h1 );
    denominator_legend_label.push_back( label );

    return 0;
  }

  /*
   * Reset pointer to the numerator plot
   */
  void TH2Ratio::ResetNumerator() {

    if ( h1_numerator ) {
      h1_numerator = NULL;
      numerator_legend_label = "";
    }
  }

  /*
   * Reset vector of denominator plots
   */
  void TH2Ratio::ResetDenominator() {

    if ( h1_denominator.size() > 0 ) {
      h1_denominator.clear();
      denominator_legend_label.clear();
    }
  }


  /*
   * Create and return comparison distribution based on plot type.
   */
  void TH2Ratio::MakeComparisonDistribution() {
 
    if ( !HistogramsAreReady() ) return;
      
    if ( numerator_axis_label == "" ) numerator_axis_label = "Data";
    if ( denominator_axis_label == "" ) denominator_axis_label = "MC";
   
    TH1* h1_totalDenominator = (TH1F*)h1_denominator.at(0)->Clone("h1_totalMC_temp");
    h1_totalDenominator->Reset();
    for ( auto *h : h1_denominator )
      h1_totalDenominator->Add( h );
    
    string ratio_name = h1_numerator->GetName();
    ratio_name += "_ratio";
 
    if ( x_label == "" )
      x_label = h1_numerator->GetXaxis()->GetTitle();

    switch( plot_type ) {
      case Type::ratio: 
        h1_ratio = (TH1F*)h1_numerator->Clone( ratio_name.c_str() );
        h1_ratio->Divide( h1_totalDenominator );
        h1_ratio->SetTitle(Form(";%s;%s / %s", x_label.c_str(), numerator_axis_label.c_str(), denominator_axis_label.c_str()));
        break;
      case Type::difference: 
        h1_ratio = (TH1F*)h1_numerator->Clone( ratio_name.c_str() );
        h1_ratio->Add( h1_totalDenominator, -1 );
        h1_ratio->SetTitle(Form(";%s;%s - %s", x_label.c_str(), numerator_axis_label.c_str(), denominator_axis_label.c_str()));
        break;
      case Type::percent_difference: 
        h1_ratio = (TH1F*)h1_numerator->Clone( ratio_name.c_str() );
        h1_ratio->Add( h1_totalDenominator, -1 );
        h1_ratio->Divide( h1_totalDenominator );
        h1_ratio->Scale( 100. );
        h1_ratio->SetTitle(Form(";%s;(%s-%s)/%s (%%)", x_label.c_str(), numerator_axis_label.c_str(), denominator_axis_label.c_str(), denominator_axis_label.c_str()));
        break;
    }

    SetComparisonRange();
    struct AH::TH1Format comparisonFormat;
    AH::FormatTH1( h1_ratio, comparisonFormat );
    
    delete h1_totalDenominator;
  } 
  
  /*
   * Draw the comarison plot. Modifies input pad if they are not initialized.
   */
  Int_t TH2Ratio::Draw ( TCanvas *can, TPad* top_pad, TPad* bottom_pad, string op ) {
  
    // Check that histograms are ready to be printed
    if ( !HistogramsAreReady() ) 
      return -1;

    // Create TPads if null
    if ( !top_pad ) top_pad = MakeTopPad();
    if ( !bottom_pad ) bottom_pad = MakeBottomPad();
    
    // Parse line_option for setting Grid option
    top_pad->SetGrid( line_option & 0b1000, line_option & 0b0010 );
    bottom_pad->SetGrid( line_option & 0b0100, line_option & 0b0001 );
    
    // Scale histograms if option is set
    if ( shape_analysis ) {
      Int_t nDen = 0;
      for ( auto* h1 : h1_denominator )
        nDen += h1->Integral();   
      if ( nDen > 0 )
        for ( auto* h1 : h1_denominator )
          h1->Scale( 1. / (Float_t)nDen );
      
      AH::Normalize( h1_numerator );
    }
    else if ( scale_to_numerator ) {
      Int_t nDen = 0;
      for ( auto* h1 : h1_denominator )
        nDen += h1->Integral();   
      if ( nDen > 0 )
        for ( auto* h1 : h1_denominator )
          h1->Scale( h1_numerator->Integral() / (Float_t)nDen );
    }

    if ( !h1_ratio )
      MakeComparisonDistribution();

    // Legend
    Float_t leg_low = 0.83 - 0.04 * ( h1_denominator.size() + 1 );
    TLegend *leg = new TLegend( 0.7, leg_low, 0.89, 0.89 );
    
    // Stack of denominator plots
    string stack_name = "stack";
    stack_name += h1_numerator->GetName();

    THStack *stack_denominator = new THStack( stack_name.c_str(), "" );
    string leg_op;
    if ( !op.find("p") ) leg_op = "pe";
    else leg_op = "lf";
    leg->AddEntry( h1_numerator, numerator_legend_label.c_str(), leg_op.c_str() );
    for ( size_t i = 0; i < h1_denominator.size(); i++ ) {
      stack_denominator->Add( h1_denominator.at(i) );
      leg->AddEntry( h1_denominator.at(i), denominator_legend_label.at(i).c_str(), "lf" ); 
    }

    Float_t ymax = 1.3 * h1_numerator->GetMaximum();
    ymax = std::max( ymax, (Float_t)stack_denominator->GetMaximum() );
    stack_denominator->SetMaximum( ymax );

    can->cd();
    top_pad->Draw();
    top_pad->cd();
    stack_denominator->Draw( "hist" );
    // X axis
    stack_denominator->GetXaxis()->SetTitleSize( 0 );
    stack_denominator->GetXaxis()->SetLabelSize( 0 );
    // Y axis
    stack_denominator->GetYaxis()->SetTitleSize( 0.053 );
    stack_denominator->GetYaxis()->SetTitleOffset( 1 );
    stack_denominator->GetYaxis()->SetLabelSize( 0.053 );
    stack_denominator->GetYaxis()->SetMaxDigits( 3 );
   
    stack_denominator->GetYaxis()->SetTitle("Events");
    
    h1_numerator->Draw( (op + " same").c_str() );
    leg->Draw();
    top_pad->RedrawAxis();
   
    can->cd();
    bottom_pad->Draw();
    bottom_pad->cd();
    h1_ratio->Draw( "pe" );
    // X axis
    h1_ratio->GetXaxis()->SetTitleSize( 0.11 );
    h1_ratio->GetXaxis()->SetTitleOffset( 0.8 );
    h1_ratio->GetXaxis()->SetLabelSize( 0.10 );
    // Y axis
    h1_ratio->GetYaxis()->SetNdivisions( 505 );
    h1_ratio->GetYaxis()->SetLabelSize( 0.10 );
    h1_ratio->GetYaxis()->SetTitleSize( 0.09 );
    h1_ratio->GetYaxis()->SetTitleOffset( 0.57 );
    h1_ratio->GetYaxis()->SetMaxDigits( 3 );
    
    return 0;
  }

  /*
   * Create pad for numerator and denominator distributions
   */
  TPad* TH2Ratio::MakeTopPad() {
  
    TPad* top_pad = new TPad( "top_pad", "", 0., 0.37, 1., 1. );
    top_pad->SetBottomMargin( 0.025 );

    return top_pad;
  }

  /*
   * Create pad for comparison plot
   */
  TPad* TH2Ratio::MakeBottomPad() {

    TPad* bottom_pad = new TPad( "bottom_pad", "", 0., 0., 1., 0.35 );
    bottom_pad->SetTopMargin( 0.03 );
    bottom_pad->SetBottomMargin( 0.22 );

    return bottom_pad;
  }

  /*
   * Check that the histogram h1 is compatible with currently added histograms.
   */
  Bool_t TH2Ratio::IsGoodBinning( TH1* h1 ) {
    
    if ( h1_numerator && h1_numerator->GetNbinsX() != h1->GetNbinsX() )
      return false;
    else if ( h1_denominator.size() > 0 && h1_denominator.at(0)->GetNbinsX() != h1->GetNbinsX() )
      return false;
    else
      return true;
  }

  /*
   * Sets the comparison plot y-axis range. If no paramaters are given, automatically set good range.
   */
  void TH2Ratio::SetComparisonRange( Float_t ylow, Float_t yhigh ) {

    if ( !h1_ratio ) {
      cerr << "Warning:\tCannot set range of comparison plot because it has not been constructed. Call GetComparisonDistribution() first." << endl;
      return;
    }

    if ( ylow != 0 || yhigh != 0 ) {
      if ( ylow < yhigh ) {
        h1_ratio->SetMaximum( yhigh );
        h1_ratio->SetMinimum( ylow );
        return;
      }
      else {
        cerr << "Warning:\tInput range order for comparison distribution" << h1_ratio->GetName() << " will be reversed." << endl;
        h1_ratio->SetMaximum( ylow );
        h1_ratio->SetMinimum( yhigh );
        return;
      }
    }

    // If ylow and yhigh are both 0, i.e. default parameters
    // Find reasonable bounds on comparison plot
    Int_t nBins = h1_ratio->GetNbinsX();
    
    // Add all bin contents to a vector
    std::vector< Float_t > vals;
    for ( Int_t bin = 1; bin <= nBins; bin++ ) 
      if ( ( plot_type == Type::ratio && h1_ratio->GetBinContent(bin) > 0) || plot_type != Type::ratio )
        vals.push_back( h1_ratio->GetBinContent(bin) );
    std::sort( vals.begin(), vals.end() );
    if ( vals.size() == 0 ) vals.push_back(0);

    // Compute percentiles if there are a large number of bins and values
    Int_t nValues = vals.size();
    if ( nValues > nBins/2 && nBins > 15 ) {
      Float_t median = 0;
      Float_t centralPercentile = 0;
      Float_t dHalfPercentile = 0.25; // Can't set this close to 0.5
      if ( nValues % 2 == 0) {
        median = vals.at( nValues/2 - 1 );
        centralPercentile = ( vals.at( nValues/2-1 + nValues*dHalfPercentile ) 
                                - vals.at( nValues/2-1 - nValues*dHalfPercentile ) ) / 2;
      }
      else {
        median = ( vals.at( (nValues+1)/2 - 1 ) + vals.at( (nValues-1)/2 - 1) ) / 2;
        centralPercentile = ( vals.at( (nValues+1)/2-1 + nValues*dHalfPercentile ) 
                                - vals.at( (nValues-1)/2-1 - nValues*dHalfPercentile ) ) / 2;
      }
      
      // Enforce a minimum plotting range of +/-1%
      centralPercentile = std::max( centralPercentile, (Float_t)0.02 );
      h1_ratio->SetMaximum( median + 4*centralPercentile);
      h1_ratio->SetMinimum( median - 4*centralPercentile);
    }

    // Set range to include all points if there are few points
    else {
      h1_ratio->SetMaximum( vals.back()  + .4*( vals.back() - vals.front() ) );
      h1_ratio->SetMinimum( vals.front() - .4*( vals.back() - vals.front() ) );
    }
    h1_ratio->SetStats(0);
  }


  /*
   * Set the grid x on TPads with bitstring
   */
  void TH2Ratio::SetVerticalLines( Int_t option ) {
    
    Int_t mask = 0b0011;
    line_option = ( line_option & mask ) | ( option << 2 );
  }

  /*
   * Set the grid y on TPads with bitstring
   */
  void TH2Ratio::SetHorizontalLines( Int_t option ) {
    
    Int_t mask = 0b1100;
    line_option = ( line_option & mask ) | option;
  }

  /*
   * Check that there is a numerator plot and at least one denominator plot.
   */
  Bool_t TH2Ratio::HistogramsAreReady() {
    
    if ( !h1_numerator ) {
      cerr << "Warning:\tNumerator histogram is not defined. Cannot do comparison" << endl;
      return false;
    }
    else if ( h1_denominator.size() == 0 ) {
      cerr << "Warning:\tDenominator histograms are not defined. Cannot do comparison" << endl;
      return false;
    }
    else {
      for ( auto *h : h1_denominator ) 
        if ( !h ) {
          cerr << "Warning:\tOne of the denominator histograms is null. Cannot do comparison" << endl;
          return false;
        }
    }

    return true;
  }

  /*
   * Return the number of bins in the histogram(s)
   */
  Int_t TH2Ratio::GetNBins() {
    if ( h1_numerator ) return h1_numerator->GetNbinsX();
    if ( h1_denominator.size() > 0 ) return h1_denominator.at(0)->GetNbinsX();
    cerr << "Warning:\tGetNbins() called when no histograms were added - returning zero" << endl;
    return 0;
  }
}

#endif
