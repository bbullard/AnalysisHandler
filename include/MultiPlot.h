#ifndef MULTI_PLOT_H
#define MULTI_PLOT_H

#include <iostream>
#include <iomanip>
#include "TLegend.h"

using namespace std;

namespace AH {
  
  class MultiPlot {
    
    /*
     * MultiPlot class declaration
     */
    public:
      MultiPlot( string name = "" );
      ~MultiPlot();

      void AddPlot( TH1* h1, string label );
      Int_t Draw( TCanvas *can );
      Float_t** GetStats( Int_t power = 2, Bool_t print = false );
      vector< string > GetLabel() { return label_vector; }

    private:
      string name;
      vector< TH1* > h1_vector;
      vector< string > label_vector;
      Float_t LinearSeparation( TH1* h1_A, TH1* h1_B );
      Float_t QuadraticSeparation( TH1* h1_A, TH1* h1_B );
      Bool_t canGetStats;
  };

  /*
   * Constructor. Set the name of the multiplot.
   */
  MultiPlot::MultiPlot( string name ) {
    
    this->name = name;
    canGetStats = true;
  }

  /*
   * Add a plot, check if the binning agrees to record if binwise comparison is still available
   */
  void MultiPlot::AddPlot( TH1* h1, string label ) {
   
    if ( h1_vector.size() > 0 )
      canGetStats = canGetStats && h1_vector.back()->GetNbinsX() == h1->GetNbinsX();
    
    h1_vector.push_back( h1 );
    label_vector.push_back( label );
  }

  /*
   * Return 2D array of separation power for all pairs of histograms
   */
  Float_t** MultiPlot::GetStats( Int_t power, Bool_t print ) {
  
    if ( power != 1 && power != 2 ) {
      cerr << "Warning:\tOnly supported separation functions are linear (power = 1) and quadratic (power = 2). Setting power to 2." << endl;
      power = 2;
    }

    Int_t nPlots = h1_vector.size();
    Float_t** stats = new Float_t*[nPlots];
    for ( Int_t i = 0; i < nPlots; i++ )
      stats[i] = new Float_t[nPlots];
    
    if ( canGetStats ) {
      for ( Int_t a = 0; a < nPlots; a++ )
        for (Int_t b = a+1; b < nPlots; b++ )
          switch( power ){
            case 1:
              stats[a][b] = LinearSeparation( h1_vector.at(a), h1_vector.at(b) );
              break;
            case 2:
              stats[a][b] = QuadraticSeparation( h1_vector.at(a), h1_vector.at(b) );
              break;
          }
    }
    else
      cerr << "Warning:\tBinning of histograms is not equal, so a comparison cannot be done." << endl;
    
    if ( print ) {
      std::cout << std::fixed;
      std::cout << std::setprecision(3);
      cout << "\nInfo:\tPrinting stats for MultiPlot " << name << ":" << endl;
      for ( Int_t a = 0; a < nPlots; a++ )
        cout << "\t" << label_vector.at(a);
      cout << endl << "\t";
      for ( Int_t a = 0; a < nPlots; a++ )
        cout << "-------";
      for ( Int_t a = 0; a < nPlots; a++ ) {
        cout << endl << label_vector.at(a) << " | ";
        for ( Int_t b = 0; b < nPlots; b++ ) {
          if ( b < a )
            cout << "   -   \t";
          else if ( b == a )
            cout << " 0 \t";
          else
            cout << stats[a][b] << "\t";
        }
      }
      cout << endl;
    }

    return stats;
  }

  /*
   * Compute the linear separation of two histograms
   */
  Float_t MultiPlot::LinearSeparation( TH1* h1_A, TH1* h1_B ) {
    
    TH1* A = (TH1F*)h1_A->Clone("A");
    TH1* B = (TH1F*)h1_B->Clone("B");
    A->Scale( 1 / A->Integral() );
    B->Scale( 1 / B->Integral() );
    A->Add( B, -1 );

    for ( Int_t bin = 1; bin <= A->GetNbinsX(); bin++ )
      A->SetBinContent( bin, abs( A->GetBinContent( bin ) ) );
  
    return A->Integral() / 2;
  }

  /*
   * Compute the quadratic separation of two histograms
   */
  Float_t MultiPlot::QuadraticSeparation( TH1* h1_A, TH1* h1_B ) {
    
    TH1* A = (TH1F*)h1_A->Clone("A");
    TH1* B = (TH1F*)h1_B->Clone("B"); 
    A->Scale( 1 / A->Integral() );
    B->Scale( 1 / B->Integral() );

    TH1* diff = (TH1F*)A->Clone("diff");
    diff->Add( B, -1 );
    diff->Multiply( diff );
    TH1* sum = (TH1F*)A->Clone("sum");
    sum->Add( B );

    diff->Divide( sum );

    return diff->Integral() / 2;
  }

  /*
   * Draw the multiplot with good formatting and a legend
   */
  Int_t MultiPlot::Draw( TCanvas *can ) {
    
    can->cd(1);
    Int_t nPlots = h1_vector.size();
    
    Float_t leg_low = 0.89 - 0.05*nPlots;
    leg_low = leg_low > 0 ? leg_low : 0;

    TLegend* leg = new TLegend( 0.7, leg_low, 0.89, 0.89 );
    for ( Int_t i = 0; i < nPlots; i++ )
      leg->AddEntry( h1_vector.at(i), label_vector.at(i).c_str(), "lf" );

    Float_t ymax = 0;
    for ( auto* h1 : h1_vector ) 
      if ( h1->GetMaximum() > ymax )
        ymax = h1->GetMaximum();
    
    h1_vector.at(0)->SetMaximum( 1.2 * ymax );
    h1_vector.at(0)->SetMinimum( 0 );

    h1_vector.at(0)->Draw( "hist" );
    for ( Int_t i = 1; i < nPlots; i++ )
      h1_vector.at(i)->Draw( "hist same" );
    leg->Draw();
  
    return 0;
  }
}

#endif
