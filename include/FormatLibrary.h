#ifndef FORMAT_LIBRARY_H
#define FORMAT_LIBRARY_H

#include "TH1.h"
#include "TGraph.h"
#include "TLine.h"

namespace AH {

  struct TH1Format {
    UInt_t line_color = kBlack;
    UInt_t line_width = 2;
    UInt_t line_style = 1;
    UInt_t fill_color = 0;
    UInt_t fill_style = 0;
    Float_t fill_alpha = 1;
    UInt_t marker_color = kBlack;
    UInt_t marker_size = 1;
    UInt_t marker_style = 8;
  };

  struct TGraphFormat {
    UInt_t line_color = kBlack;
    UInt_t line_width = 2;
    UInt_t line_style = 1;
    UInt_t marker_color = kBlack;
    UInt_t marker_size = 1;
    UInt_t marker_style = 8;
  };

  struct TLineFormat {
    UInt_t line_color = kBlack;
    UInt_t line_width = 2;
    UInt_t line_style = 1;
  };
  
  void FormatTH1( TH1* h1, TH1Format form ) {
    
    h1->SetLineColor( form.line_color );
    h1->SetLineWidth( form.line_width );
    h1->SetLineStyle( form.line_style );
    h1->SetFillColorAlpha( form.fill_color, form.fill_alpha );
    h1->SetFillStyle( form.fill_style );
    h1->SetMarkerColor( form.marker_color );
    h1->SetMarkerSize( form.marker_size );
    h1->SetMarkerStyle( form.marker_style );
  }
  
  void FormatTGraph( TGraph* g1, TGraphFormat form ) {
    
    g1->SetLineColor( form.line_color );
    g1->SetLineWidth( form.line_width );
    g1->SetLineStyle( form.line_style );
    g1->SetMarkerColor( form.marker_color );
    g1->SetMarkerSize( form.marker_size );
    g1->SetMarkerStyle( form.marker_style );
  }

  void FormatTLine( TLine* l, TLineFormat form ) {
  
    l->SetLineColor( form.line_color );
    l->SetLineWidth( form.line_width );
    l->SetLineStyle( form.line_style );
  }
}

#endif
