#ifndef ISAMPLE_HANDLER_H
#define ISAMPLE_HANDLER_H

#include <string>
#include <iostream>
#include <fstream>
#include "TFile.h"
#include "TH1.h"
#include "TH2.h"
#include "TChain.h"
#include "TDirectory.h"
#include "FormatLibrary.h"
#include "LoadingBar.h"
#include "CutFlow.h"

using namespace std;

namespace AH {

  class ISampleHandler {	
  
    public:
      ISampleHandler( string ntuple_name, string data_list = "", Bool_t isBatch = false );
      virtual ~ISampleHandler();

      void    AddFile( string file_name ) { ntuple->Add( file_name.c_str() ); nEntries = ntuple->GetEntries(); }
      void    Initialize( string label );
      Int_t   Process( Bool_t test = false ); 
      Bool_t  Add( ISampleHandler* SH );

      UInt_t  GetNEntries() { return nEntries; }
      Float_t GetNEventsPreselection() { return cutflow->GetStart(); }
      Float_t GetNEvents() { return cutflow->GetEnd(); }
      string  GetName() { return sample_label; }
      AH::CutFlow*  GetCutflow() { return cutflow; }
      Bool_t  IsProcessed() { return isProcessed; }
      void    SetBatch( Bool_t setbatch = true ) { batch = setbatch; }

      TH1F* h1_cutflow;
  
    protected:
      virtual void ConnectBranches() = 0;
      virtual void InitializeHistograms() = 0; 
      virtual inline void FillHistograms() {};
      virtual inline void ClearVectors() {};
      void WriteCutflow();
      void FormatCutflow( AH::TH1Format form );
      void ScaleCutflow( Float_t scale );

      TChain *ntuple;
      AH::CutFlow *cutflow;

      string sample_label;
      UInt_t nEntries;
      Bool_t isInitialized;
      Bool_t isProcessed;
      Bool_t batch;
  };

  /*
   * Constructor. Receives file name and name of ntuple to build a TChain
   */
  ISampleHandler::ISampleHandler( string ntuple_name, string data_list, Bool_t isBatch ) {  
  
    ntuple = new TChain( ntuple_name.c_str() );
    if ( data_list != "" ) {
      if ( !isBatch )
        cout << "Info:\tRetrieving ntuple " << ntuple_name << " from files in data list " << data_list << endl;
      
      ifstream inFile( data_list.c_str() );
      string data_file;
      while ( getline( inFile, data_file ) )
        ntuple->Add( data_file.c_str() );
    }
    else if ( !isBatch )
      cout << "Info:\tSaving ntuple name " << ntuple_name << " now, must add files later." << endl;
    
    sample_label = "";
    isProcessed = false;
    isInitialized = false;
    batch = isBatch;
    h1_cutflow = nullptr;
  }

  /*
   * Destructor. Deletes the TChain
   */
  ISampleHandler::~ISampleHandler() {
   
    if (ntuple) delete ntuple;
    if (h1_cutflow) delete h1_cutflow;
  }

  /*
   * Initialize histograms
   */
  void ISampleHandler::Initialize( string label ) {
    
    sample_label = label;
    InitializeHistograms();
    isInitialized = true;
    nEntries = ntuple->GetEntries();
    cutflow = new AH::CutFlow( label );
  }
  
  Bool_t ISampleHandler::Add( ISampleHandler *SH ) {
     
    if ( SH->IsProcessed() ) {
      isProcessed = true; 
      nEntries += SH->GetNEvents();
      cutflow->Add( SH->GetCutflow() );
      if ( h1_cutflow ) { delete h1_cutflow; h1_cutflow = nullptr; }
      h1_cutflow = cutflow->GetHistogram();
      return true;
    }
    else {
      cout << "Warning:\tISampleHandler " << SH->GetName() << " not yet processed, not adding to " << GetName() << endl;
      return false;
    }
  }
      
  void ISampleHandler::WriteCutflow() { 
    if( h1_cutflow ) 
      h1_cutflow->Write(); 
  }

  void ISampleHandler::FormatCutflow( AH::TH1Format form ) { 
    
    if( h1_cutflow ) 
      FormatTH1( h1_cutflow, form ); 
  }

  void ISampleHandler::ScaleCutflow( Float_t scale ) { 
   
    cutflow->Scale( scale );
    if( h1_cutflow )
      delete h1_cutflow;
    h1_cutflow = (TH1F*)cutflow->GetHistogram();
  }

  /*
   * Initialize branches and loop over TChain
   */
  Int_t ISampleHandler::Process( Bool_t test ) {
  
    if ( isProcessed ) {
      cerr << "Warning:\tISampleHandler already processed or combined with another ISampleHandler. Exiting." << endl;
      return -1;
    }

    if ( !isInitialized ) {
      cerr << "Warning:\tISampleHandler is not initialized, cannot process ntuple. Exiting." << endl;
      return -2;
    }

    ConnectBranches();

    nEntries = ntuple->GetEntries();
    UInt_t nEventMax = test ? 10000 : nEntries;
    
    LoadingBar b( nEntries );
    
    if ( !batch) cout << "Info:\tBegin event loop over " << sample_label << " ntuple - " << nEventMax << " events" << endl;
    for ( UInt_t i = 0; i < nEventMax; i++ ) {
      if ( !batch ) b.Increment();
      ClearVectors();
      ntuple->GetEntry( i );
      FillHistograms();
    }
    if ( !batch ) b.Finish();
    isProcessed = true;
  
    if ( h1_cutflow ) { delete h1_cutflow; h1_cutflow = nullptr; }
    h1_cutflow = cutflow->GetHistogram();

    return 0;
  } 
}

#endif
