#include <iostream>
#include <string>
#include <vector>
#include "TH1.h"
#include "TMath.h"

using namespace std;

namespace AH {
  
  class CutFlow {

    public:
      CutFlow( string name ) { this->name = name; h1_cutflow = nullptr; }
      ~CutFlow() { if ( h1_cutflow ) delete h1_cutflow; }
    
      void Fill( string cut, Float_t weight = 1. );
      void Add( CutFlow *cf );
      TH1F* GetHistogram();
      void Scale( Float_t scale );
  
      string GetName() { return name; }
      vector< string > GetCuts() { return cuts; }
      vector< Float_t > GetCounts() { return counts; }
      vector< Float_t > GetSumw2() { return sumw2; }
      
      Float_t GetStart() { return (counts.size()>0) ? counts.at(0) : 0; }
      Float_t GetEnd() { return (counts.size()>0) ? counts.back() : 0; }
  
    private:
      string name;
      vector< string > cuts;
      vector< Float_t > counts;
      vector< Float_t > sumw2;
      TH1F* h1_cutflow;
  };
  
  void CutFlow::Fill( string cut, Float_t weight ) {

    vector< string >::iterator it = find( cuts.begin(), cuts.end(), cut );
    if ( it == cuts.end() ) { 
      cuts.push_back( cut );
      counts.push_back( weight );
      sumw2.push_back( weight*weight );
    }
    else {
      size_t i_cut = distance( cuts.begin(), it );
      counts.at( i_cut ) += weight;
      sumw2.at( i_cut ) += weight*weight;
    }
  }

  void CutFlow::Add( CutFlow *cf ) {
   
    vector< string > cf_cuts = cf->GetCuts();
    vector< Float_t > cf_counts = cf->GetCounts();
    vector< Float_t > cf_sumw2 = cf->GetSumw2();

    if ( !cf_cuts.size() ) {
      cerr << "Warning:\t Cannot add cutflow " << cf->GetName() << " - it's empty!" << endl;
      return;
    }
    
    if ( !cuts.size() ) {
      cuts = cf_cuts;
      counts = cf_counts;
      sumw2 = cf_sumw2;
      return;
    }

    // Check that the cuts are common and in the right order, even if some are missing at the end of one cutflow
    size_t n = 0;
    while ( n < min( cf_cuts.size(), cuts.size() ) && !cuts.at(n).compare(cf_cuts.at(n)) )
      n++;
    if ( n < min( cf_cuts.size(), cuts.size() ) ) {
      cerr << "Warning:\t Cannot add cutflow " << cf->GetName() << " with size " << cf_cuts.size() << " to cutflow " << name << " with size " << cuts.size() << endl
            << "\t\tOnly the first " << n << " cuts are common" << endl;
      return;
    }

    for ( size_t i = 0; i < cf_cuts.size(); i++ ) {
      vector< string >::iterator it = find( cuts.begin(), cuts.end(), cf_cuts.at(i) );
      if ( it == cuts.end() ) { 
        cuts.push_back( cf_cuts.at(i) );
        counts.push_back( cf_counts.at(i) );
        sumw2.push_back( cf_sumw2.at(i) );
      }
      else {
        size_t i_cut = distance( cuts.begin(), it );
        counts.at( i_cut ) += cf_counts.at(i);
        sumw2.at( i_cut ) += cf_sumw2.at(i);
      }
    }
  }

  void CutFlow::Scale( Float_t scale ) {
  
    for ( size_t i = 0; i < cuts.size(); i++ ) {
      counts.at(i) *= scale;
      sumw2.at(i) *= scale;
    }
    if ( h1_cutflow ) { delete h1_cutflow; h1_cutflow = nullptr; }
  }

  TH1F* CutFlow::GetHistogram() {
   
    if ( !cuts.size() ) {
      cout << "Warning:\t Cutflow is empty, returning null pointer!" << endl;
      return NULL;
    }
  
    if ( h1_cutflow ) {
      delete h1_cutflow;
      h1_cutflow = nullptr;
    }
  
    h1_cutflow = new TH1F( (name+"_h1_cutflow").c_str(), "", cuts.size(), 0, cuts.size() );
    h1_cutflow->GetXaxis()->SetLabelSize(0.05); 
 
    for ( UInt_t bin = 1; bin <= cuts.size(); bin++ ) {
      h1_cutflow->GetXaxis()->SetBinLabel( bin, cuts.at(bin-1).c_str() );
      h1_cutflow->SetBinContent( bin, counts.at(bin-1) );
      h1_cutflow->SetBinError( bin, TMath::Sqrt( sumw2.at(bin-1) ) );
    }
    
    //h1_cutflow->GetSumw2()->Set(0);
    h1_cutflow->SetTitle(";;Events");

    return (TH1F*)h1_cutflow->Clone();
  }
}
