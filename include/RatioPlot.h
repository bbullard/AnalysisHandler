#ifndef RATIO_PLOT_H
#define RATIO_PLOT_H

#include <string>
#include <tuple>
#include "TPad.h"
#include "TCanvas.h"
#include "TH1.h"
#include "TLegend.h"
#include "THStack.h"
#include "Utility.h"
#include "FormatLibrary.h"

using namespace std;

namespace AH {
  
  class RatioPlot {	
  
    enum Type { ratio, difference, percent_difference };

    /*
     * RatioPlot class declaration. Handles a variable number of numerator histograms and plots them independently.
     * Handles a variable number of denominator plots that are taken together as a stacked histogram.
     */
    public:
      RatioPlot( Type t = Type::ratio, Bool_t no_clone = false );
      ~RatioPlot();
      
      Int_t AddNumerator( TH1 *h1, string label );
      Int_t AddDenominator( TH1 *h1, string label );
      void ResetNumerator();
      void ResetDenominator(); 

      void SetXaxisLabel( string label ) { x_label = label; }
      void SetYaxisLabels( string num_label, string den_label ) { numerator_axis_label = num_label; denominator_axis_label = den_label; }
      
      void SetVerticalLines( Int_t option = 0b11 );
      void SetHorizontalLines( Int_t option = 0b01 );
      void SetScaleToNumerator( Bool_t option = true );
      void SetScaleToDenominator( Bool_t option = true );
      void SetScaleToUnity( Bool_t option = true );
      void SetComparisonRange( Float_t ymin = 0, Float_t ymax = 0 );
      
      TPad* MakeTopPad();
      TPad* MakeBottomPad();
      Int_t Draw( TCanvas *can, TPad *top_pad = 0, TPad* bottom_pad = 0, Bool_t num_line_plot = false, Bool_t no_legend = false );
      
      vector< TH1* > GetComparisonDistribution() { MakeComparisonDistribution(); return h1_ratio; }
      Int_t GetNBins();
  
    private:
      void  MakeComparisonDistribution();
      
      Bool_t HistogramsAreReady();
      Bool_t IsGoodBinning( TH1* h1 );

      vector< TH1* > h1_numerator;
      vector< string > numerator_legend_label;
      string numerator_axis_label;
      
      vector< TH1* > h1_denominator;
      vector< string > denominator_legend_label;
      string denominator_axis_label;
      
      vector< TH1* > h1_ratio;
      Type plot_type;
      Bool_t no_clone;

      string x_label;	
      Int_t line_option;
      Bool_t scale_to_unity;
      Bool_t scale_to_numerator;
      Bool_t scale_to_denominator;
  };

  /*
   * Constructor. Initialize meta-data
   */
  RatioPlot::RatioPlot( Type t, Bool_t no_clone ) {  
    plot_type = t;
    this->no_clone = no_clone;
    numerator_legend_label = vector< string >(0);
    denominator_legend_label = vector< string >(0);
    numerator_axis_label = "";
    denominator_axis_label = "";
    line_option = 0;

    scale_to_unity = false;
    scale_to_numerator = false;
    scale_to_denominator = false;
  }

  /*
   * Destructor. Delete ratio plot
   */
  RatioPlot::~RatioPlot() {
    
    for ( auto* h1: h1_ratio ) {
      delete h1; h1 = nullptr;
    }

    if ( !no_clone ) {
      for ( auto* h1 : h1_numerator ) {
        delete h1; h1 = nullptr;
      }
      for ( auto* h1 : h1_denominator ) {
        delete h1; h1 = nullptr;
      }
    }
  }

  /*
   * Set histogram in numerator, check for overwrite
   */
  Int_t RatioPlot::AddNumerator( TH1 *h1, string label ) {

    if ( !IsGoodBinning( h1 ) ) {
      cerr << "Warning:\tCannot use histogram " << h1->GetName() << " - inconsistent binning" << endl;
      return -1;
    }

    TH1 *h1_save;
    if ( no_clone )
      h1_save = h1;
    else 
      h1_save = (TH1*)h1->Clone();

    h1_numerator.push_back( h1_save );
    numerator_legend_label.push_back( label );

    return 0;
  }

  /*
   * Add histogram in the denominator
   */
  Int_t RatioPlot::AddDenominator( TH1 *h1, string label ) {

    if ( !IsGoodBinning( h1 ) ) {
      cerr << "Warning:\tCannot use histogram " << h1->GetName() << " - inconsistent binning" << endl;
      return -1;
    }

    TH1 *h1_save;
    if ( no_clone )
      h1_save = h1;
    else 
      h1_save = (TH1*)h1->Clone();
    
    h1_denominator.push_back( h1_save );
    denominator_legend_label.push_back( label );

    return 0;
  }

  /*
   * Reset pointer to the numerator plot
   */
  void RatioPlot::ResetNumerator() {

    if ( h1_numerator.size() ) {
      if ( !no_clone ) {
        for ( auto* h1: h1_numerator )
        { delete h1; h1 = nullptr; }
      }
      h1_numerator.clear();
      numerator_legend_label.clear();
    }
  }

  /*
   * Reset vector of denominator plots
   */
  void RatioPlot::ResetDenominator() {

    if ( h1_denominator.size() ) {
      if ( !no_clone ) {
        for ( auto* h1: h1_denominator )
        { delete h1; h1 = nullptr; }
      }
      h1_denominator.clear();
      denominator_legend_label.clear();
    }
  }

  /*
   * Draw the comarison plot. Modifies input pad if they are not initialized.
   */
  Int_t RatioPlot::Draw ( TCanvas *can, TPad* top_pad, TPad* bottom_pad, Bool_t num_line_plot , Bool_t no_legend ) {
  
    // Check that histograms are ready to be printed
    if ( !HistogramsAreReady() ) 
      return -1;

    // Create TPads if null
    if ( !top_pad ) top_pad = MakeTopPad();
    if ( !bottom_pad ) bottom_pad = MakeBottomPad();
   
    // Parse line_option for setting Grid option
    top_pad->SetGrid( line_option & 0b1000, line_option & 0b0010 );
    bottom_pad->SetGrid( line_option & 0b0100, line_option & 0b0001 );
    
    // Scale histograms if option is set
    if ( scale_to_unity ) {
      Float_t nDen = 0;
      for ( auto* h1 : h1_denominator )
        nDen += h1->Integral();   
      if ( nDen > 0 )
        for ( auto* h1 : h1_denominator )
          h1->Scale( 1. / nDen );
      
      for ( auto* h1 : h1_numerator )
        AH::Normalize( h1 );
    }
    else if ( scale_to_numerator ) {
      Float_t nDen = 0;
      for ( auto* h1 : h1_denominator )
        nDen += h1->Integral();   
      if ( nDen > 0 )
        for ( auto* h1 : h1_denominator )
          h1->Scale( h1_numerator.at(0)->Integral() / nDen );
      for ( auto* h1 : h1_numerator )
        AH::Normalize( h1, h1_numerator.at(0)->Integral() );
    }
    else if ( scale_to_denominator ) {
      Float_t nDen = 0;
      for ( auto* h1 : h1_denominator )
        nDen += h1->Integral();   
      for ( auto* h1 : h1_numerator )
        AH::Normalize( h1, nDen );
    }

    if ( !h1_ratio.size() )
      MakeComparisonDistribution();

    // Legend
    Float_t leg_low = 0.85 - 0.05 * ( h1_denominator.size() + h1_numerator.size() );
    TLegend *leg = new TLegend( 0.7, leg_low, 0.89, 0.89 );
    
    // Stack of denominator plots
    string stack_name = "stack";
    stack_name += h1_numerator.at(0)->GetName();

    THStack *stack_denominator = new THStack( stack_name.c_str(), "" );
    
    string num_draw_op = "", num_leg_op = "", den_leg_op = "", ratio_op = "";
    if ( h1_numerator.size() == 1 ) {
      if ( !num_line_plot ) {
        num_draw_op = "pe";
        num_leg_op = "pe";
      }
      else {
        num_draw_op = "hist";
        if ( h1_numerator.at(0)->GetFillStyle() )
          num_leg_op = "f";
        else
          num_leg_op = "l";
      }
      h1_ratio.at(0)->SetLineColor( kBlack );
      ratio_op = "pe";
    }
    else {
      for ( auto* h1 : h1_ratio )
        h1->SetMarkerStyle(0);
      num_draw_op = "hist";
      ratio_op = "e hist";
      if ( h1_numerator.at(0)->GetFillStyle() )
        num_leg_op = "f";
      else
        num_leg_op = "l";
    }
    if ( h1_denominator.at(0)->GetFillStyle() )
      den_leg_op = "f";
    else
      den_leg_op = "l";

    for ( size_t i = 0; i < h1_denominator.size(); i++ ) {
      stack_denominator->Add( h1_denominator.at(i) );
      leg->AddEntry( h1_denominator.at(i), denominator_legend_label.at(i).c_str(), den_leg_op.c_str() ); 
    }

    for ( size_t i = 0; i < h1_numerator.size(); i++ )
      leg->AddEntry( h1_numerator.at(i), numerator_legend_label.at(i).c_str(), num_leg_op.c_str() );
    
    Float_t ymax = 0;
    for ( auto *h1 : h1_numerator )
      ymax = h1->GetMaximum() > ymax ? h1->GetMaximum() : ymax;
    ymax = 1.3 * std::max( ymax, (Float_t)stack_denominator->GetMaximum() );
    stack_denominator->SetMaximum( ymax );

    // Set title based on the denominator plot
    h1_ratio.at(0)->GetXaxis()->SetTitle( h1_denominator.at(0)->GetXaxis()->GetTitle() );

    can->cd();
    top_pad->Draw();
    top_pad->cd();
    stack_denominator->Draw( "hist" );
    // X axis
    stack_denominator->GetXaxis()->SetTitleSize( 0 );
    stack_denominator->GetXaxis()->SetLabelSize( 0 );
    // Y axis
    stack_denominator->GetYaxis()->SetTitleSize( 0.053 );
    stack_denominator->GetYaxis()->SetTitleOffset( 1 );
    stack_denominator->GetYaxis()->SetLabelSize( 0.053 );
    stack_denominator->GetYaxis()->SetMaxDigits( 3 );
   
    stack_denominator->GetYaxis()->SetTitle("Events");
   
    for ( auto *h1 : h1_numerator )
      h1->Draw( ( num_draw_op + " same").c_str() );
    if ( !no_legend ) leg->Draw();
    top_pad->RedrawAxis();
   
    can->cd();
    bottom_pad->Draw();
    bottom_pad->cd();
    h1_ratio.at(0)->Draw( ratio_op.c_str() );
    for ( size_t i = 1; i < h1_ratio.size(); i++ )
      h1_ratio.at(i)->Draw( ("same " + ratio_op).c_str() );
    // X axis
    h1_ratio.at(0)->GetXaxis()->SetTitleSize( 0.11 );
    h1_ratio.at(0)->GetXaxis()->SetTitleOffset( 0.8 );
    h1_ratio.at(0)->GetXaxis()->SetLabelSize( 0.10 );
    // Y axis
    h1_ratio.at(0)->GetYaxis()->SetNdivisions( 505 );
    h1_ratio.at(0)->GetYaxis()->SetLabelSize( 0.10 );
    h1_ratio.at(0)->GetYaxis()->SetTitleSize( 0.09 );
    h1_ratio.at(0)->GetYaxis()->SetTitleOffset( 0.57 );
    h1_ratio.at(0)->GetYaxis()->SetMaxDigits( 3 );
    
    return 0;
  }

  /*
   * Create and return comparison distribution based on plot type.
   */
  void RatioPlot::MakeComparisonDistribution() {
 
    if ( h1_ratio.size() ) return;

    if ( !HistogramsAreReady() ) return;
      
    if ( numerator_axis_label == "" ) numerator_axis_label = "Data";
    if ( denominator_axis_label == "" ) denominator_axis_label = "MC";
   
    TH1* h1_totalDenominator = (TH1F*)h1_denominator.at(0)->Clone("h1_totalDenominator_temp");
    h1_totalDenominator->Reset();
    for ( auto *h : h1_denominator )
      h1_totalDenominator->Add( h );
    
    if ( x_label == "" )
      x_label = h1_numerator.at(0)->GetXaxis()->GetTitle();

    for ( auto *h1_num : h1_numerator ) {
      string n = h1_num->GetName();
      switch( plot_type ) {
        case Type::ratio: 
          h1_ratio.push_back( (TH1F*)h1_num->Clone((n+"_ratio").c_str()) );
          h1_ratio.back()->Divide( h1_totalDenominator );
          h1_ratio.back()->SetTitle(Form(";%s;%s / %s", x_label.c_str(), numerator_axis_label.c_str(), denominator_axis_label.c_str()));
          break;
        case Type::difference: 
          h1_ratio.push_back( (TH1F*)h1_num->Clone((n+"_ratio").c_str()) );
          h1_ratio.back()->Add( h1_totalDenominator, -1 );
          h1_ratio.back()->SetTitle(Form(";%s;%s - %s", x_label.c_str(), numerator_axis_label.c_str(), denominator_axis_label.c_str()));
          break;
        case Type::percent_difference: 
          h1_ratio.push_back( (TH1F*)h1_num->Clone((n+"_ratio").c_str()) );
          h1_ratio.back()->Add( h1_totalDenominator, -1 );
          h1_ratio.back()->Divide( h1_totalDenominator );
          h1_ratio.back()->Scale( 100. );
          h1_ratio.back()->SetTitle(Form(";%s;(%s-%s)/%s (%%)", x_label.c_str(), numerator_axis_label.c_str(), denominator_axis_label.c_str(), denominator_axis_label.c_str()));
          break;
      }
      h1_ratio.back()->SetFillStyle(0);
    }

    SetComparisonRange();
    
    delete h1_totalDenominator;
  } 
  
  /*
   * Sets the comparison plot y-axis range. If no paramaters are given, automatically set good range.
   */
  void RatioPlot::SetComparisonRange( Float_t ymin, Float_t ymax ) {

    if ( !h1_ratio.size() ) {
      cerr << "Warning:\tCannot set range of comparison plot(s) because it has not been constructed. Call GetComparisonDistribution() first." << endl;
      return;
    }

    if ( ymin != 0 || ymax != 0 ) {
      if ( ymin < ymax ) {
        h1_ratio.at(0)->SetMaximum( ymax );
        h1_ratio.at(0)->SetMinimum( ymin );
        return;
      }
      else {
        cerr << "Warning:\tInput range order for comparison distribution" << h1_ratio.at(0)->GetName() << " will be reversed." << endl;
        h1_ratio.at(0)->SetMaximum( ymin );
        h1_ratio.at(0)->SetMinimum( ymax );
        return;
      }
    }

    // If ymin and ymax are both 0, i.e. default parameters
    // Find reasonable bounds on comparison plot
    Int_t nBins = h1_ratio.at(0)->GetNbinsX();
    
    for ( auto* h1 : h1_ratio ) {
      // Add all bin contents to a vector
      std::vector< Float_t > vals;
      for ( Int_t bin = 1; bin <= nBins; bin++ ) 
<<<<<<< HEAD
        if ( ( plot_type == Type::ratio && h1->GetBinContent(bin) > 0) || plot_type != Type::ratio )
=======
        if ( h1->GetBinContent(bin) > 0 )
>>>>>>> e813e5b6efaec618e11463b1c5d5dd329f97192c
          vals.push_back( h1->GetBinContent(bin) );
      std::sort( vals.begin(), vals.end() );
      if ( vals.size() == 0 ) vals.push_back(0);

      // Compute percentiles if there are a large number of bins and values
      Int_t nValues = vals.size();
      if ( nValues > nBins/2 && nBins > 15 ) {
        Float_t median = 0;
        Float_t centralPercentile = 0;
        Float_t dHalfPercentile = 0.25; // Can't set this close to 0.5
        if ( nValues % 2 == 0) {
          median = vals.at( nValues/2 - 1 );
          centralPercentile = ( vals.at( nValues/2-1 + nValues*dHalfPercentile ) 
                                  - vals.at( nValues/2-1 - nValues*dHalfPercentile ) ) / 2;
        }
        else {
          median = ( vals.at( (nValues+1)/2 - 1 ) + vals.at( (nValues-1)/2 - 1) ) / 2;
          centralPercentile = ( vals.at( (nValues+1)/2-1 + nValues*dHalfPercentile ) 
                                  - vals.at( (nValues-1)/2-1 - nValues*dHalfPercentile ) ) / 2;
        }
<<<<<<< HEAD
        
        // Enforce a minimum plotting range of +/-1%
        centralPercentile = std::max( centralPercentile, (Float_t)0.02 );
        h1->SetMaximum( median + 4*centralPercentile);
        h1->SetMinimum( median - 4*centralPercentile);
=======
       
        Float_t mean = 0;
        for ( auto v : vals )
          mean += v / (Float_t)nValues;
        
        // Enforce a minimum plotting range of +/-1%
        centralPercentile = std::max( centralPercentile, (Float_t)0.01 );
        
        //Float_t center = median + centralPercentile * atan( log10( 2* (mean-median) / (mean+median) )) / ( TMath::Pi() / 2 );
        //h1->SetMaximum( median + 4*centralPercentile);
        //h1->SetMinimum( median - 4*centralPercentile);
        //h1->SetMaximum( mean + 4*centralPercentile);
        //h1->SetMinimum( mean - 4*centralPercentile);
        Float_t yDown = min( mean, median) - 4*centralPercentile;
        if ( mean-median < -0.01 ) yDown = vals.at(0) - centralPercentile;
        h1->SetMaximum( min( mean, median) + 4*centralPercentile );
        h1->SetMinimum( yDown );
>>>>>>> e813e5b6efaec618e11463b1c5d5dd329f97192c
      }

      // Set range to include all points if there are few points
      else {
        h1->SetMaximum( vals.back()  + .4*( vals.back() - vals.front() ) );
        h1->SetMinimum( vals.front() - .4*( vals.back() - vals.front() ) );
      }
      h1->SetStats(0);
    }

    ymax = h1_ratio.at(0)->GetMaximum();
    ymin = h1_ratio.at(0)->GetMinimum();
    for ( auto* h1: h1_ratio ) {
      ymax = h1->GetMaximum() > ymax ? h1->GetMaximum() : ymax;
      ymin = h1->GetMinimum() < ymin ? h1->GetMinimum() : ymin;
    }
    h1_ratio.at(0)->SetMaximum( ymax );
    h1_ratio.at(0)->SetMinimum( ymin );
  }

  /*
   * Create pad for numerator and denominator distributions
   */
  TPad* RatioPlot::MakeTopPad() {
  
    TPad* top_pad = new TPad( "top_pad", "", 0., 0.37, 1., 1. );
    top_pad->SetBottomMargin( 0.025 );

    return top_pad;
  }

  /*
   * Create pad for comparison plot
   */
  TPad* RatioPlot::MakeBottomPad() {

    TPad* bottom_pad = new TPad( "bottom_pad", "", 0., 0., 1., 0.35 );
    bottom_pad->SetTopMargin( 0.03 );
    bottom_pad->SetBottomMargin( 0.22 );

    return bottom_pad;
  }

  /*
   * Check that the histogram h1 is compatible with currently added histograms.
   */
  Bool_t RatioPlot::IsGoodBinning( TH1* h1 ) {
    
    if ( h1_numerator.size() && h1_numerator.at(0)->GetNbinsX() != h1->GetNbinsX() )
      return false;
    else if ( h1_denominator.size() && h1_denominator.at(0)->GetNbinsX() != h1->GetNbinsX() )
      return false;
    else
      return true;
  }

  /*
   * Set the grid x on TPads with bitstring
   */
  void RatioPlot::SetVerticalLines( Int_t option ) {
    
    Int_t mask = 0b0011;
    line_option = ( line_option & mask ) | ( option << 2 );
  }

  /*
   * Set the grid y on TPads with bitstring
   */
  void RatioPlot::SetHorizontalLines( Int_t option ) {
    
    Int_t mask = 0b1100;
    line_option = ( line_option & mask ) | option;
  }

  void RatioPlot::SetScaleToNumerator( Bool_t option ) {
   
    scale_to_numerator = option;
    if ( scale_to_numerator ) {
      SetScaleToDenominator( false );
      SetScaleToUnity( false );
    }
  }

  void RatioPlot::SetScaleToDenominator( Bool_t option ) {

    scale_to_denominator = option;
    if ( scale_to_denominator ) {
      SetScaleToNumerator( false );
      SetScaleToUnity( false );
    }
  }
  
  void RatioPlot::SetScaleToUnity( Bool_t option ) {

    scale_to_unity = option;
    if ( scale_to_unity ) {
      SetScaleToDenominator( false );
      SetScaleToNumerator( false );
    }
  }

  /*
   * Check that there is a numerator plot and at least one denominator plot.
   */
  Bool_t RatioPlot::HistogramsAreReady() {
    
    if ( !h1_numerator.size() ) {
      cerr << "Warning:\tNumerator histograms are not defined. Cannot do comparison" << endl;
      return false;
    }
    else if ( !h1_denominator.size() ) {
      cerr << "Warning:\tDenominator histograms are not defined. Cannot do comparison" << endl;
      return false;
    }
    else {
      for ( auto *h : h1_numerator ) 
        if ( !h ) {
          cerr << "Warning:\tOne of the numerator histograms is null. Cannot do comparison" << endl;
          return false;
        }
      for ( auto *h : h1_denominator ) 
        if ( !h ) {
          cerr << "Warning:\tOne of the denominator histograms is null. Cannot do comparison" << endl;
          return false;
        }
    }

    return true;
  }

  /*
   * Return the number of bins in the histogram(s)
   */
  Int_t RatioPlot::GetNBins() {
    if ( h1_numerator.size() ) return h1_numerator.at(0)->GetNbinsX();
    if ( h1_denominator.size() ) return h1_denominator.at(0)->GetNbinsX();
    cerr << "Warning:\tGetNbins() called when no histograms were added - returning zero" << endl;
    return 0;
  }
}

#endif
