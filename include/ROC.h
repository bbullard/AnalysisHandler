#ifndef ROC_H
#define ROC_H

#include <iostream>
#include <string>
#include "TPad.h"
#include "TCanvas.h"
#include "TH1.h"
#include "TLine.h"
#include "TLegend.h"
#include "FormatLibrary.h"

using namespace std;

namespace AH {
 
  class ROC {	
  
    /*
     * ROC class declaration
     */
    public:
      ROC( TH1* h1_a, TH1* h1_b );
      ~ROC();
      
      void SetLabels( string a = "A", string b = "B" ) { A_label = a; B_label = b; g1_ROC->SetTitle( Form(";#epsilon_{%s};1-#epsilon_{%s}", A_label.c_str(), B_label.c_str()) );}
      void SetROCFormat( AH::TGraphFormat form );
      Int_t Draw( TCanvas *can, Bool_t drawComparison = false, const char* op = "AC" );
      TGraph* GetROC() { return g1_ROC; }

    private:
      TH1* h1_A;
      TH1* h1_B;
      TGraph *g1_ROC;

      string A_label;
      string B_label;
  };
  
  /*
   * Constructor. Initialize histograms for comparison. Build ROC.
   */
  ROC::ROC( TH1* h1_a, TH1* h1_b ) {
    
    UInt_t nBins;
    if ( h1_a->GetNbinsX() != h1_b->GetNbinsX() ) {
      cerr << "Warning:\tHistograms " << h1_a->GetName() << " and " << h1_b->GetName() << " do not have equal number of bins. Cannot create ROC." << endl;
      g1_ROC = NULL;
      return;
    } 

    nBins = h1_a->GetNbinsX();
    h1_A = h1_a;
    h1_B = h1_b;

    Float_t A_integral = h1_A->Integral();
    Float_t B_integral = h1_B->Integral();

    Float_t A_eff[nBins], B_rej[nBins];
    for ( UInt_t bin = 1; bin <= nBins; bin++ ) {
      A_eff[ bin-1 ] = h1_A->Integral(1, bin) / A_integral;
      B_rej[ bin-1 ] = 1 - h1_B->Integral(1, bin) / B_integral;
    }

    g1_ROC = new TGraph( nBins, A_eff, B_rej ) ;
    SetLabels();
    g1_ROC->GetHistogram()->SetMaximum( 1 );
    g1_ROC->GetHistogram()->SetMinimum( 0 );
    g1_ROC->GetXaxis()->SetLimits( 0, 1 );
  }

  /*
   * Destructor. Delete TGraph for ROC
   */
  ROC::~ROC() {
    
    if ( g1_ROC ) delete g1_ROC;
  }

  /*
   * Set the format of the TGraph 
   */
  void ROC::SetROCFormat( AH::TGraphFormat form ) {
    
    AH::FormatTGraph( g1_ROC, form );
  }

  /*
   * Draw the ROC curve with the option of also drawing the two distributions on the same axes.
   */
  Int_t ROC::Draw( TCanvas *can, Bool_t drawComparison, const char* op ) {
   
    h1_A->Scale( 1 / h1_A->Integral() );
    h1_B->Scale( 1 / h1_B->Integral() );
    
    Bool_t isDivided = ( can->cd(1) != can->cd(2) );
    can->cd(1);
    if ( drawComparison ) {
      if ( !isDivided ) { 
        cerr << "Warning:\tCanvas " << can->GetName() << " is not divided when division is expected. Dividing into 2 TVirtualPads." << endl;
        can->Divide( 2, 1 );
      }
      can->cd(1);
      
      TLegend* leg = new TLegend( 0.6, 0.8, 0.89, 0.89 );
      leg->AddEntry( h1_A, A_label.c_str(), "lf" );
      leg->AddEntry( h1_B, B_label.c_str(), "lf" );

      float ymax = std::max ( h1_A->GetMaximum(), h1_B->GetMaximum() );
      h1_A->SetMaximum( ymax * 1.1 );
      h1_A->Draw( "hist" );
      h1_B->Draw( "hist same" );
      leg->Draw();
      can->cd(2);
    }
    else {
      if ( isDivided ) 
        cerr << "Warning:\tCanvas " << can->GetName() << " is divided when no division is expected. ROC plot for " << h1_A->GetName() << " will look bad." << endl;
      can->cd(1);
    }

    can->SetGrid();
    TLine* diagonal =  new TLine( 0, 1, 1, 0 );
    struct AH::TLineFormat line_form;
    line_form.line_style = 2;
    AH::FormatTLine( diagonal, line_form );

    g1_ROC->Draw( op );
    diagonal->Draw();
   
    return 0;
  }
}

#endif
