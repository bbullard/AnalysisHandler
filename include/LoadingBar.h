#ifndef LOADING_BAR_H
#define LOADING_BAR_H

#include <iostream>

using namespace std;

namespace AH {

  class LoadingBar {
    
    public:
      LoadingBar( Int_t nIterations, Int_t barLength = 100 );
      
      inline void Increment( Int_t inc = 1 );
      void Finish() { nIter = 0; cout << endl << endl; }

    private:
      Int_t nIter;
      Int_t iter;
      Int_t barLength;
  };

  LoadingBar::LoadingBar( Int_t nIterations, Int_t barLength ) {
    
    this->nIter = nIterations;
    this->barLength = barLength;
    iter = 0;
  } 

  void LoadingBar::Increment( Int_t inc ) {
    
    Int_t prog_1 = (Int_t) ( (Float_t)iter / (Float_t)nIter * (Float_t)barLength );
    Int_t prog_2 = (Int_t) ( (Float_t)(iter+inc) / (Float_t)nIter * (Float_t)barLength );
    iter += inc;
    
    // Check if incrementing modifies the loading bar
    if ( iter > inc && prog_1 == prog_2 ) return;

    cout << "[";
    for ( Int_t i = 0; i < barLength; i++ ) {
      if ( i < prog_2 ) cout << "#";
      else cout << " ";
    }
    cout << "] " << (Int_t)( (Float_t)prog_2 / (Float_t)barLength * 100. ) << " %\r";
    cout.flush();
  }
}

#endif
