#ifndef UTILITY_H
#define UTILITY_H

#include <string>
#include "TH1.h"
#include "TPaveText.h"

using namespace std;

namespace AH {
  
  TH1* Normalize( TH1* h, Float_t scaleTo = 1. ) { 
   
    if ( h->Integral() > 0 )
      h->Scale( scaleTo / h->Integral() ); 
    return h; 
  }

  void FormatTPaveText( TPaveText *tp, string op = "left" ) {
    
    tp->SetTextSize(26);
    tp->SetTextFont(43);
    tp->SetBorderSize(0);
    tp->SetFillColor(0);
    if ( op.compare( "left" ) == 0 ) 
      tp->SetTextAlign( 12 );
    else if ( op.compare( "center" ) == 0 ) 
      tp->SetTextAlign( 22 );
    else if ( op.compare( "right" ) == 0 ) 
      tp->SetTextAlign( 32 );
  }
} 

#endif
