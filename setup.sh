#!/bin/bash

if [ -d /cvmfs/ ]; then
    echo "Setting up ATLAS environment quietly ..."
    setupATLAS > quiet_setup.txt
    echo "Setting up CMake quietly ..."
    lsetup cmake >> quiet_setup.txt
    echo "Setting up gcc quietly ..."
    lsetup "gcc gcc493_x86_64_slc6" >> quiet_setup.txt
    echo "Setting up ROOT quietly ..."
    lsetup "root 6.18.04-x86_64-centos7-gcc8-opt" >> quiet_setup.txt
    rm quiet_setup.txt
fi

if [ ! -d "./plots" ]
then 
  mkdir plots
fi

if [ ! -s "./conf" ]
then
  mkdir conf
fi

if [ ! -s "./root_dump" ]
then
  mkdir root_dump
fi

if [ ! -s "./data_list" ]
then
  mkdir data_list
fi

echo ""
echo " -- Double check to make sure you're on the right branch and tag --"
echo "BRANCHES:" 
git branch
echo ""
echo "TAGS:"
git tag
echo ""
