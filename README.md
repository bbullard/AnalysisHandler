# AnalysisHandler
Package for streamlined plotting and analysis from ROOT ntuples with minimimal low-level management.

## Setup
Before beginning a new project, consider creating a new branch of the repository and/or a new tag. The recommended organization is to separate projects with branches and tags for versions of that project.

**1) Environment.** Run `source setup.sh`.  This will setup ROOT and gcc, as well as create the directories `conf`, `plots`, and `data_list` if they are not already present. 

**2) Ntuple configuration file.** Run `python scripts/make_configuration.py --inFile /path/to/example_file.root --ntuple ntuple_name [--confName conf_file_name]`. 
This will generate a YAML configuration file for all branches in the ntuple `ntuple` in the ROOT file `inFile`. The argument `confName` is the name of the created configuration file. 
If it is not given, the default value is the name of the ntuple. The fields for each branch in the configuration file are: `type`, the C++ data type of the branch; `link`, a bit to indicate if this branch should be linked; 
`plot`, a bit to indicate if this branch should be plotted in a TH1. If `plot` is enabled, the `link` bit is irrelevant and the following fields become relevant: `binning`, 
an array of the form `[nbins, xlow, xhigh]`; `duplicates`, an array of strings that define copies of this branche's histograms and are used as suffixes for the histogram's name.

**3) Make class.** Run `python scripts/make_class.py --ntupleConf conf/configuration.yml --className NameOfClass [--overWrite]`. This will create a class definition named `className` 
in a file `include/className.h` based on the configuration file `ntupleConf`. The flag `--overWrite` is required when a file `className.h` is already present, and the user 
is prompted to fulfill the overwrite.  

**4) Data lists.** Run `python scripts/make_data_list.py --dataDir /path/to/sample/root/files --outFile name_of_data_list.txt`. This creates a text file `outFile` in the folder `data_list` that contains all of the files inside of `dataDir` recursively in order of file size. 

**5) Write analysis code.** Edit the file `include/className.h` and `Run.cxx` to do your analysis and plotting. 

**6) Build and run.** Run `make` to build and `./Run [test]` to run. The optional argument string `test` can be given to run over a limited number of events for quick testing.


## Headers
`ISampleHandler.h`: base class for sample handlers

`MTSampleHandler.h`: handler for processing a dataset in multithreading mode

`RatioPlot.h`: class for creating comparison distributions for two TH1s. Capable of ratio, difference, and fractional difference comparisons.

`MultiPlot.h`: class for plotting multiple distributions on the same axes neatly and computing separation metrics for all distributions.

`ROC.h`: class for plotting receiver-operator-curve for two 1-dimensional distributions

`LoadingBar.h`: class for printing a loading bar

`FormatLibrary.h`: list of structs defining histogram and graph formatting options and functions to apply those to the corresponding object

`Utility.h`: useful functions


## Scripts
`make_test_ntuple.cxx`: can be executed in a ROOT session to create a test ntuple for developing the package.

`skim_ntuple.py --inDir /path/to/inDir --outDir /path/to/outDir`: skims ntuples in files in `inDir` based on branch values and writes the skimmed ntuples in directory `outDir`.

`plot_separation_metrics.py`: plots different 1-dimensional distribution separation metrics for Gaussians

