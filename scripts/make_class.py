import os.path
import sys
import argparse
import yaml

parser = argparse.ArgumentParser()
parser.add_argument('--ntupleConf', type=str, required=True, help='Location of configuration file')
parser.add_argument('--className', type=str, required=True, help='Name of the class that will be created')
parser.add_argument('--overwrite', action='store_true', help='Flag that allows class definition to be overwritten')
args = parser.parse_args()

def prompt_user():
  options = {"yes": True, "y": True,
             "no": False, "n": False}
  print ""
  prompt = "Are you sure you want to overwrite file include/" + args.className + ".h? [y/n] "
  
  while True:
      sys.stdout.write( prompt )
      choice = raw_input().lower()
      if choice == '':
          continue
      elif choice in options:
          print ""
          return options[choice]
      else:
          sys.stdout.write("Please respond with 'yes' or 'no' "
                            "(or 'y' or 'n').\n") 

# Initializes new class
def initialize():

  print "Generating class definition for %s from configuration file %s ..." % ( args.className, args.ntupleConf )

  # Open configuration file
  with open(args.ntupleConf, 'r') as ymlfile:
    try: 
      ntupleConf = yaml.safe_load(ymlfile) or {}
    except yaml.YAMLError as exc:
      print(exc)
      return
  
  name = args.className
 
  branches = ntupleConf.keys()
  branches.sort()

  # maximum length of branch name
  max_length = 0
  for branch in ntupleConf.keys():
    if ( ntupleConf[branch]['link']+ntupleConf[branch]['plot'] == 0 ):
      continue
    if len(branch) > max_length:
      max_length = len(branch)
    for clone in ntupleConf[branch]['duplicates']:
      if len(branch+clone) > max_length:
        max_length = len(branch+clone)
 
  max_length = max_length +1

  th1_declarations = ""
  delete_th1 = ""
  th1_inits = ""
  th1_labels = ""
  th1_clones = ""
  th1_format = ""
  th1_scale = ""
  th1_dump = ""
  th1_sumw2 = ""
  th1_fill = ""
  th1_add = ""

  scalar_declarations = ""
  vector_declarations = ""
  new_vectors = ""
  delete_vectors = ""
  clear_vectors = ""
  connect_branches = ""
  for branch in branches:
    if ntupleConf[branch]['plot']:
      binning = ntupleConf[branch]['binning']
      th1_declarations = th1_declarations + "\n      TH1* h1_%s;" % branch
      delete_th1 = delete_th1 + "\n    if ( h1_%s delete h1_%s;" % ( ('%s )'%branch).ljust(max_length+2), branch )
      th1_inits = th1_inits + "\n    h1_%s = new TH1F( %s \"\", %d, %.2f, %.2f );" % ( branch.ljust(max_length), ('(sample_label+"_h1_%s").c_str(),' % branch).ljust(max_length), binning[0], binning[1], binning[2] )
      th1_labels = th1_labels + '\n    h1_%s->SetTitle( ";%s;Events" );' % ( branch.ljust(max_length), branch )
      th1_format = th1_format + '\n    FormatTH1( h1_%s form );' % (branch+',').ljust(max_length+1)
      th1_scale = th1_scale + '\n    h1_%s->Scale( scale );' % branch.ljust(max_length)
      th1_dump = th1_dump + '\n    h1_%s->Write();' % branch.ljust(max_length)
      th1_sumw2 = th1_sumw2 + '\n    h1_%s->Sumw2();' % branch.ljust(max_length)
      th1_fill = th1_fill + '\n    h1_%s->Fill( %s );' % ( branch.ljust(max_length), branch )
      th1_add = th1_add + '\n    h1_%s->Add( SH->h1_%s );' % ( branch.ljust(max_length), branch )
      for clone in ntupleConf[branch]['duplicates']:
        th1_declarations = th1_declarations + "\n      TH1* h1_%s_%s;" % ( branch, clone )
        delete_th1 = delete_th1 + "\n    if ( h1_%s delete h1_%s;" % ( ('%s )'%(branch+'_'+clone)).ljust(max_length+2), (branch+'_'+clone) )
        th1_clones = th1_clones + '\n    h1_%s_%s = (TH1F*)h1_%s->Clone( (sample_label+"_h1_%s_%s").c_str() );' % ( branch, clone, branch, branch, clone )
        th1_format = th1_format + '\n    FormatTH1( h1_%s form );' % (branch+'_'+clone+',').ljust(max_length)
        th1_scale = th1_scale + '\n    h1_%s->Scale( scale );' % (branch+'_'+clone).ljust(max_length)
        th1_dump = th1_dump + '\n    h1_%s->Write();' % ( branch+'_'+clone).ljust(max_length)
        th1_fill = th1_fill + '\n    h1_%s->Fill( %s );' % ( (branch+'_'+clone).ljust(max_length), branch )
        th1_add = th1_add + '\n    h1_%s->Add( SH->h1_%s_%s );' % ( (branch+'_'+clone).ljust(max_length), branch, clone )


    if ntupleConf[branch]['link'] or ntupleConf[branch]['plot']:
      connect_branches = connect_branches + "\n    ntuple->SetBranchAddress( %s &%s );" % ( ('"%s",' % branch).ljust(max_length+4), branch )
      if ntupleConf[branch]['type'].find( 'vector' ) >= 0:
        new_vectors = new_vectors + "\n      %s = new %s(0);" % ( branch.ljust(max_length), ntupleConf[branch]['type'] )
        delete_vectors = delete_vectors + '\n    if ( %s delete %s;' % ( ('%s )'%branch).ljust(max_length+2), branch )
        clear_vectors = clear_vectors + "\n    %s->clear();" % branch
        vector_declarations = vector_declarations + "\n      %s* %s;" % ( ntupleConf[branch]['type'].ljust( 17 ), branch )
      else:
        scalar_declarations = scalar_declarations + "\n      %s %s;" % ( ntupleConf[branch]['type'].ljust( 7 ), branch )
  
  boilerplate_header = '''\
#ifndef %s_H
#define %s_H

#include "ISampleHandler.h"
#include "FormatLibrary.h"
#include <string>

using namespace std;

namespace AH {
  
  class %s : public ISampleHandler {
    
    public:
      %s( string ntuple_name, string data_list = "", Bool_t isBatch = false );
      ~%s();
      
      void Dump( string out_file = "" );
      Bool_t Add( %s* SH );
      void FormatAllHistograms( AH::TH1Format form );
      void ScaleAllHistograms( Float_t scale );

      // TH1 DECLARATION%s

    private:
      void ConnectBranches();
      void InitializeHistograms();
      inline void FillHistograms();
      inline void ClearVectors();
      
      // SCALAR BRANCH DECLARATION%s

      // VECTOR BRANCH DECLARATION%s
  };

''' % ( name.upper(), name.upper(), name, name, name, name, th1_declarations, scalar_declarations, vector_declarations )

  boilerplate_constDest = '''\

  %s::%s( string ntuple_name, string data_list, Bool_t isBatch )
    : ISampleHandler( ntuple_name, data_list, isBatch ) 
    {
      
      // VECTOR INITIALIZATION%s
    }

  %s::~%s() {
    
    // DELETE TH1S%s

    // DELETE VECTORS%s
  }

''' % ( name, name, new_vectors, name, name, delete_th1, delete_vectors )
  
  boilerplate_connectBranches = '''\
  void %s::ConnectBranches() {
    
    // SET BRANCH ADDRESSES%s
  }

''' % ( name, connect_branches )

  boilerplate_clearVectors = '''\
  void %s::ClearVectors() {
    
    // CLEAR VECTOR VALUES%s
  }

''' % ( name, clear_vectors )

  boilerplate_initializeHistograms = '''\
  void %s::InitializeHistograms() {
   
    // INITIALIZE TH1S%s
   
    // SET SUMW2%s

    // SET TH1 TITLES%s
    
    // CLONE TH1S FOR ADDITONAL CUTS%s 
  }

''' % ( name, th1_inits, th1_sumw2, th1_labels, th1_clones )

  boilerplate_formatHistograms = '''\
  void %s::FormatAllHistograms( AH::TH1Format form ) {
    
    ISampleHandler::FormatCutflow( form );
  %s
  }

''' % ( name, th1_format )

  boilerplate_scaleHistograms = '''\
  void %s::ScaleAllHistograms( Float_t scale ) {
    
    ISampleHandler::ScaleCutflow( scale );
  %s
  }

''' % ( name, th1_scale )

  boilerplate_dump = '''\
  void %s::Dump( string out_file ) {
  
    if ( out_file == "" ) 
      out_file = sample_label + ".root";

    TFile* file = new TFile( ( "root_dump/" + out_file ).c_str(), "RECREATE" );
    WriteCutflow();%s
    file->Close();
  }

''' % ( name, th1_dump )

  boilerplate_add = '''\
  Bool_t %s::Add( %s* SH ) {
    
    if ( !ISampleHandler::Add( SH ) )
      return false;
    %s

    return true;
  }

''' % ( name, name, th1_add )

  boilerplate_fillHistograms = '''\
  // Called every iteration of the loop over the ntuple
  inline void %s::FillHistograms() {

    cutflow->Fill( "Start" );
    // DEFAULT FILLING%s

    /*
    * YOUR CODE HERE
    */
  }
}

#endif
''' % ( name, th1_fill )

  out_file = open( "include/%s.h" % name, "w" )
  out_file.write( boilerplate_header 
                  + boilerplate_constDest 
                  + boilerplate_connectBranches 
                  + boilerplate_clearVectors 
                  + boilerplate_initializeHistograms 
                  + boilerplate_formatHistograms
                  + boilerplate_scaleHistograms
                  + boilerplate_dump
                  + boilerplate_add
                  + boilerplate_fillHistograms )
  out_file.close()

  print "\nNew class ", name, " defined in include/%s.h. Consider an inital commit:" % name
  print "git add include/%s.h" % name
  print "git commit -am \"Initial commit for class %s\"" % name
  print ""


# Steering script, protects from overwriting files
def steer():

  # Check to see if file exists
  if os.path.isfile( 'include/' + args.className + '.h' ):
    if ( args.overwrite ):
      print "\nFile include/%s.h already exists. Consider committing current changes and adding a tag:" % args.className
      print "git commit -am \"Final commit for tag <tag>\""
      print "git tag -a <tag> -m \"Tag description\""
      if prompt_user():
        print "Overwriting file include/%s.h" % args.className
        initialize()
      else:
        print "Not overwriting file include/%s.h" % args.className
    else:
      print "\nOverwrite flag not given - class definition will not be generated."
  else:
    initialize()


if __name__ == '__main__':
  steer()
