from scipy.integrate import quad
import numpy as np
import matplotlib.pyplot as plt

def get_separation( f, g, degree = 1, xlow = -10, xhigh = 20 ):
  
  if degree < 1:
    print "Invalid degree. Setting to 1."
    degree = 1;
    
  def combined( x ):
    if ( f(x) + g(x) < 1e-5 ):
      return 0
    if degree == 1:
      return 0.5 * abs( f(x) - g(x) )
    else:
      return 0.5 * ( abs( f(x) - g(x) )**degree / ( f(x) + g(x) )**(degree-1) )

  return quad( combined, xlow, xhigh )[0]

def plot_separation():
  
  def get_gaus( mu, sig ):
    def gaus( x ):
      return 1 / np.sqrt( 2 * np.pi * sig**2 ) * np.exp( - ( x - mu )**2 / (2 * sig**2 ) )
    return gaus

  def get_const( low, high ):
    def const( x ):
      if ( low < x < high ):
        return 1 / ( high - low )
      else:
        return 0
    return const

  dx = np.linspace( 0, 5, 100 )
  linear_separation = [ get_separation( get_gaus( 0, 1 ), get_gaus( x, 1 ), degree = 1 ) for x in dx ]
  quadratic_separation = [ get_separation( get_gaus( 0, 1 ), get_gaus( x, 1 ), degree = 2 ) for x in dx ]
  #linear_separation = [ get_separation( get_const( 0, 5 ), get_const( x, x+5 ), 1, 0, x+5 ) for x in dx ]
  #quadratic_separation = [ get_separation( get_const( 0, 5 ), get_const( x, x+5 ), 2, 0, x+5 ) for x in dx ]

  plt.plot( dx, linear_separation, '-r', label='Degree 1' )
  plt.plot (dx, quadratic_separation, '-b', label='Degree 2' )
  plt.legend(loc='lower right')
  plt.show()
  plt.savefig( 'const_separation.png' )

if __name__=='__main__':
  plot_separation()
