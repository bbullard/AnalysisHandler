import array, math, sys, time
import ROOT as r
import yaml
import argparse
import numpy as np

def StringBranch( branch, dic ):
  
  branch_string = '''\
%s:
  binning: [ %i, %s, %s ]
  duplicates: []
  type: %s
  link: %i
  plot: %i

''' % ( branch, 
        dic['binning'][0], dic['binning'][1], dic['binning'][2],
        dic['type'],
        dic['link'],
        dic['plot'] )

  return branch_string

def RoundOff( x ):
  if abs(x) >= 10:
    pow10 = int(np.log10( abs(x) )) - 1
    z = round( float(x) / 10**pow10)
    y = int(z * 10**pow10 )
    return y
  elif abs(x) > 0:
    z = round( x * 1000. )
    y = float(z)/1000.
    return y
  else:
    return 0

def initialize():

  parser = argparse.ArgumentParser() 
  parser.add_argument('--inFile', type=str, required=True, help='Location of the input NTuple file')
  parser.add_argument('--ntuple', type=str, required=True, help='Name of the input NTuple')
  parser.add_argument('--confName', action='store', type=str, default='', help='Name of output configuration file')
  args = parser.parse_args()

  # Load ntuple from file
  inFile = r.TFile( args.inFile )
  if not inFile:
    print "Unable to open file. Exiting."
    return
  ntuple = inFile.Get( args.ntuple )
  if not ntuple:
    print "NTuple ", args.ntuple, " not properly read from file. Exiting."
    return

  branches = set([b.GetName() for b in ntuple.GetListOfBranches()])
  branches = list(branches)
  branches.sort()

  config = {}
  # Add branches to plot configuration
  ntuple.GetEntry(0)
  for b in branches:

    # Determine data type
    if type(getattr(ntuple, b)) is int:
      vartype = 'Int_t'
    elif type(getattr(ntuple, b)) is float:
      vartype = 'Float_t'
    elif type(getattr(ntuple, b)) is bool:
      vartype = 'Bool_t'
    elif type(getattr(ntuple, b)) is str:
      vartype = 'Char_t'
    elif str(type(getattr(ntuple, b))) == "<class 'ROOT.vector<float>'>":
      vartype = 'vector< Float_t >'
    elif str(type(getattr(ntuple, b))) == "<class 'ROOT.vector<int>'>":
      vartype = 'vector< Int_t >'
    elif str(type(getattr(ntuple, b))) == "<class 'ROOT.vector<bool>'>":
      vartype = 'vector< Bool_t >'
    elif str(type(getattr(ntuple, b))) == "<class 'ROOT.vector<str>'>":
      vartype = 'vector< Char_t >'
    else:
      vartype = ''

    # Determine plotting range
    xlow = float(ntuple.GetMinimum(b))
    xhigh = float(ntuple.GetMaximum(b))
   
    if (xlow == xhigh):
      xlow = xlow - 2
      xhigh = xhigh + 2
      nbins = 5
    elif ( vartype.find('Bool_t') != -1 or vartype.find('Char_t') != -1 ):
      nbins = 2
      xlow = 0
      xhigh = 2
    elif ( vartype.find('Int_t') != -1 ):
      xhigh += 1;
      xlow = int(xlow)
      xhigh = int(xhigh)
      nbins = xhigh-xlow
      if nbins > 100:
        nbins = 40
    else:
      xlow = RoundOff(xlow)
      xhigh = RoundOff(xhigh)
      nbins = 40
      
    config[b] = { 
                  'link': 0,
                  'plot': 0,
                  'duplicates': [ ],
                  'binning': [ nbins, xlow, xhigh ],
                  'type': vartype
                }
  
  if args.confName != '':
    confName = args.confName
  else:
    confName = args.ntuple

  filename = "conf/%s.yml" % confName
  with open( filename, 'w') as outfile:
    print "Writing configuration file ", filename
    for branch in branches:
      outfile.write( StringBranch( branch, config[branch] ) )
    #yaml.dump(config, outfile, default_flow_style=False, line_break = "\n") 
    

if __name__ == '__main__':
  initialize()
