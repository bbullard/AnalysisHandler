import argparse
import os

parser = argparse.ArgumentParser() 
parser.add_argument('--dataDir', type=str, required=True, help='Directory containing root files for a dataset')
parser.add_argument('--outFile', type=str, required=True, help='Output text file containing paths to root data files')
args = parser.parse_args()

def create():
  
  files_and_sizes = []
  for ( dirpath, dirnames, filenames ) in os.walk( args.dataDir ):
    for filename in filenames:
      full_path = os.path.join( dirpath,  filename )
      files_and_sizes.append( ( full_path, os.path.getsize( full_path ) ) )

  files_and_sizes.sort( key = lambda x: x[1] )
  
  if not os.path.exists( 'data_list' ):
    os.mkdir( 'data_list' )
  
  out = open( 'data_list/' + args.outFile, "w+")
  for f in files_and_sizes:
    out.write( f[0] + '\n' )
  out.close()

if __name__ == '__main__':
  create()
