#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include <TTree.h>
#include <TFile.h>
#include <TCanvas.h>
#include <TH1.h>
#include "../include/LoadingBar.h"

using namespace std;


void connect_branch( string file_name );
void check_trees( string file_name );
<<<<<<< HEAD
=======
void compare_trees( string file_name );
>>>>>>> e813e5b6efaec618e11463b1c5d5dd329f97192c

void link_trees( string data_list ) {

  ifstream dl_file( data_list.c_str() );
  string root_file;
<<<<<<< HEAD
  while ( getline( dl_file, root_file ) )
    connect_branch( root_file );
    //check_trees( root_file );   
=======
  //bool first = true;
  while ( getline( dl_file, root_file ) ) {
    connect_branch( root_file );
    //check_trees( root_file );   
    /*if (first) {
      compare_trees( root_file );
      first = false;
    }*/
  }
>>>>>>> e813e5b6efaec618e11463b1c5d5dd329f97192c
}


void connect_branch( string file_name ){
 
    cout << "Working on sample " << file_name << endl;
    // Open file and read TTree objects
    TFile * f = new TFile( file_name.c_str(), "UPDATE" );
    TTree * nominal = static_cast<TTree*>( f->Get("nominal") );
    TTree * truth = static_cast<TTree*>( f->Get("truth") );

    // Create the hash table that connect
    //    runNumber (x) eventNumber
    // to the tree entry number. A return code less than 0 indicated failure.
    if (truth->BuildIndex("runNumber", "eventNumber") < 0){
        std::cerr << "Could not build truth index!" << std::endl;
        std::abort();
    }

    UInt_t runNumber;
    ULong64_t eventNumber;
    vector< Float_t > *mc_generator_weights = 0;

    // Attach to reco level tree and loop to match from reco to particle level
    nominal->SetBranchAddress("runNumber", & runNumber);
    nominal->SetBranchAddress("eventNumber", & eventNumber);
<<<<<<< HEAD
    TBranch *br_mcgw = nominal->Branch( "mc_generator_weights", &mc_generator_weights );
=======
    TBranch *br_mcgw =        nominal->Branch( "mc_generator_weights", &mc_generator_weights );
>>>>>>> e813e5b6efaec618e11463b1c5d5dd329f97192c

    truth->SetBranchAddress( "mc_generator_weights", &mc_generator_weights );

    UInt_t nEntries = nominal->GetEntries();
    AH::LoadingBar b( nEntries );
<<<<<<< HEAD
    TH1F* weights = new TH1F( "weights", "", 40, 0, 5 );
    Bool_t save = true;
=======
    
    Bool_t save = true;
    Int_t nbytes, nbytes_broken;
>>>>>>> e813e5b6efaec618e11463b1c5d5dd329f97192c
    for ( Long64_t i=0; i < nEntries; i++) {
        nominal->GetEntry(i);
        Int_t nBytesRead = truth->GetEntryWithIndex(runNumber, eventNumber);
        b.Increment();
        
        // No Match -> There is no particle level event for this reco level event.
        if ( nBytesRead < 0 ) {
          cout << "DID NOT READ EVENT - DON'T SAVE TREE" << endl;
          save = false;
        }
        // Matched -> Both particle level and reco level event exit.
<<<<<<< HEAD
        else {
          for ( Int_t i = 0; i < mc_generator_weights->size(); i++ ) 
            //weights->Fill( mc_generator_weights->at(i) );
            br_mcgw->Fill();
        }
    }
    b.Finish();

    /*
    TCanvas *can = new TCanvas("can", "", 600, 600 );
    weights->Draw("hist");
    can->Print( "test_weights.png" );
    */
=======
        else 
          br_mcgw->Fill();
    }
    b.Finish();

>>>>>>> e813e5b6efaec618e11463b1c5d5dd329f97192c
    if ( save )
      nominal->Write();
    
    delete f;
}


// Useful function just for checking the proportion of events that are common between two ntuples
void check_trees( string file_name ){

    // Open file and read TTree objects
    TFile * f = new TFile( file_name.c_str(), "READ" );
<<<<<<< HEAD
=======
    if (!f) std::cerr << "NO FILE" << endl;
>>>>>>> e813e5b6efaec618e11463b1c5d5dd329f97192c
    TTree * nominal = static_cast<TTree*>( f->Get("nominal") );
    TTree * truth = static_cast<TTree*>( f->Get("truth") );

    // Create the hash table that connect
    //    runNumber (x) eventNumber
    // to the tree entry number. A return code less than 0 indicated failure.
    if (truth->BuildIndex("runNumber", "eventNumber") < 0){
        std::cerr << "Could not build truth index!" << std::endl;
        std::abort();
    }

    UInt_t runNumber;
    ULong64_t eventNumber;

    // Attach to reco level tree and loop to match from reco to particle level
    nominal->SetBranchAddress("runNumber", & runNumber);
    nominal->SetBranchAddress("eventNumber", & eventNumber);

    Float_t no_match = 0;
    Float_t match = 0;

    for (Long64_t i=0; i<nominal->GetEntries(); ++i){
        nominal->GetEntry(i);
        Int_t nBytesRead = truth->GetEntryWithIndex(runNumber, eventNumber);
        
        // No Match -> There is no particle level event for this reco level event.
        if ( nBytesRead < 0 )
          no_match++;
        // Matched -> Both particle level and reco level event exit.
<<<<<<< HEAD
        else 
          match++;
    }

=======
        else {
          match++;
        }
    }

    cout << "Number of matched events: " << match << endl
         << "Number of unmatched events: " << no_match << endl;
}


// Useful function just for checking that the branches between two trees are equal
void compare_trees( string file_name ){

    // Open file and read TTree objects
    TFile * f = new TFile( file_name.c_str(), "READ" );
    TTree * nominal = static_cast<TTree*>( f->Get("nominal") );
    TTree * truth = static_cast<TTree*>( f->Get("truth") );

    // Create the hash table that connect
    //    runNumber (x) eventNumber
    // to the tree entry number. A return code less than 0 indicated failure.
    if (truth->BuildIndex("runNumber", "eventNumber") < 0){
        std::cerr << "Could not build particleLevel index!" << std::endl;
        std::abort();
    }

    UInt_t runNumber;
    ULong64_t eventNumber;
    vector< Float_t > *mc_generator_weights_nominal = 0;
    vector< Float_t > *mc_generator_weights_truth = 0;

    // Attach to reco level tree and loop to match from reco to particle level
    nominal->SetBranchAddress("runNumber", & runNumber);
    nominal->SetBranchAddress("eventNumber", & eventNumber);
    nominal->SetBranchAddress("mc_generator_weights", &mc_generator_weights_nominal);
    truth->SetBranchAddress( "mc_generator_weights", &mc_generator_weights_truth );

    TH2F *h2_EW = new TH2F("h2_EW","",40,-5,5, 40, -5,5);
    h2_EW->SetTitle("mc_generator_weights[292] (EW);nominal;particleLevel");
    TH2F *h2_EWLO1 = new TH2F("h2_EWLO1","",40,-5,5, 40, -5,5);
    h2_EWLO1->SetTitle("mc_generator_weights[294] (EWLO1);nominal;particleLevel");
    TH2F *h2_EWLO2 = new TH2F("h2_EWLO2","",40,-5,5, 40, -5,5);
    h2_EWLO2->SetTitle("mc_generator_weights[296] (EWLO1LO2);nominal;particleLevel");
    
    Float_t no_match = 0;
    Float_t match = 0;

    for (Long64_t i=0; i<nominal->GetEntries(); ++i){
        nominal->GetEntry(i);
        Int_t nBytesRead = truth->GetEntryWithIndex(runNumber, eventNumber);
        
        // No Match -> There is no particle level event for this reco level event.
        if ( nBytesRead < 0 )
          no_match++;
        // Matched -> Both particle level and reco level event exit.
        else {
          h2_EW->Fill( mc_generator_weights_nominal->at(292), mc_generator_weights_truth->at(292) );
          h2_EWLO1->Fill( mc_generator_weights_nominal->at(294), mc_generator_weights_truth->at(294) );
          h2_EWLO2->Fill( mc_generator_weights_nominal->at(296), mc_generator_weights_truth->at(296) );
          match++;
        }
    }
    
    TFile *out = new TFile( "mc_generator_weights.root", "recreate" );
    h2_EW->Write();
    h2_EWLO1->Write();
    h2_EWLO2->Write();
    out->Close();

>>>>>>> e813e5b6efaec618e11463b1c5d5dd329f97192c
    cout << "Number of matched events: " << match << endl
         << "Number of unmatched events: " << no_match << endl;
}

