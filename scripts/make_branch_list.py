import array, math, sys, time
import ROOT as r
import yaml
import argparse
import numpy as np

def initialize():

  parser = argparse.ArgumentParser() 
  parser.add_argument('--inFile', type=str, required=True, help='Location of the input NTuple file')
  parser.add_argument('--ntuple', type=str, required=True, help='Name of the input NTuple')
  parser.add_argument('--listName', action='store', type=str, default='', help='Name of output list of branches')
  args = parser.parse_args()

  # Load ntuple from file
  inFile = r.TFile( args.inFile )
  if not inFile:
    print "Unable to open file. Exiting."
    return
  ntuple = inFile.Get( args.ntuple )
  if not ntuple:
    print "NTuple ", args.ntuple, " not properly read from file. Exiting."
    return

  branches = set([b.GetName() for b in ntuple.GetListOfBranches()])
  branches = list(branches)
  branches.sort()
  
  if args.listName != '':
    listName = args.listName
  else:
    listName = args.ntuple + '_toSkip'

  filename = "%s.txt" % listName
  with open( filename, 'w') as outfile:
    print "Writing list of branches ", filename
    for branch in branches:
      outfile.write( branch+'\n' )
    

if __name__ == '__main__':
  initialize()
