#include <iostream>
#include "TFile.h"
#include "TTree.h"
#include "TRandom3.h"

void make_test_ntuple( Int_t nEvents = 10000 ) {
  
  if ( gRandom ) delete gRandom;
  gRandom = new TRandom3( 0 );
  gRandom->SetSeed( 0 );

  TFile* tfile = new TFile( "Test_NTuple.root", "RECREATE" );
  TTree* ttree = new TTree( "Test_NTuple", "Dummy NTuple for package testing" );
  
  Float_t Gaus_mu0_sig1, Gaus_mu5_sig2;
  Int_t Pois_mu10;

  ttree->Branch( "Gaus_mu0_sig1", &Gaus_mu0_sig1, "Gaus_mu0_sig1/F" );
  ttree->Branch( "Gaus_mu5_sig2", &Gaus_mu5_sig2, "Gaus_mu5_sig2/F" );
  ttree->Branch( "Pois_mu10",     &Pois_mu10,     "Pois_mu10/I" );

  for ( Int_t i = 0; i < nEvents; i++ ) {
    Gaus_mu0_sig1 = gRandom->Gaus( 0, 1 );
    Gaus_mu5_sig2 = gRandom->Gaus( 5, 2 );
    Pois_mu10 = gRandom->Poisson( 10 );
    ttree->Fill();
  }
  
  ttree->Write();
}
