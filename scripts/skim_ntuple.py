import sys, os
import ROOT as r
import argparse 

parser = argparse.ArgumentParser() 
parser.add_argument('--inDir', type=str, required=True, help='Directory containing ROOT files to be skimmed')
parser.add_argument('--outDir', type=str, required=True, help='Directory containing output files')
parser.add_argument('--slim', type=str, default='', help='Slim the output tree\'s branches down based on branches in text file')
args = parser.parse_args()

skim_ntuple_name = 'nominal'
<<<<<<< HEAD
keep_ntuples = [ 'sumWeights' ]
=======
keep_ntuples = [ 'sumWeights', 'particleLevel', 'truth' ]
>>>>>>> e813e5b6efaec618e11463b1c5d5dd329f97192c
cuts = ''
file_suffix = 'slimmed'

def steer():

  if not os.path.exists( args.outDir ):
    print 'Directory ', args.outDir, ' does not exist. Exiting.'
    return 

  file_list = []
  if os.path.isdir( args.inDir ):
    for root, dirs, files in os.walk( args.inDir, topdown=False ):
      for name in files:
        file_list.append( [ os.path.join( root, name ), name ] )
  else:
    file_list.append( [ args.inDir, args.inDir[ args.inDir.rfind( '/' )+1:] ] )

  for f in file_list:
    print 'Skimming file ', f[1], ' ...'
    out_file_name = f[1][ 0 : f[1].rfind( '.root' ) ] + '.' + file_suffix + '.root'
    out_file_name = args.outDir + "/" + out_file_name
    #print out_file_name
    
    in_file = r.TFile( f[0] )
    in_tree = in_file.Get( skim_ntuple_name )
    if not args.slim == '':
      for branch in open( args.slim, 'r' ):
        in_tree.SetBranchStatus( branch.rstrip(), 0 )

    elist = r.TEntryList("list", "")
    in_tree.Draw(">>list", cuts, "entrylist")
    in_tree.SetEntryList(elist)
   
    kept_ntuples = [ in_file.Get( n ) for n in keep_ntuples ] 
    copied_ntuples = []
    out_file = r.TFile( out_file_name, 'recreate' )
    out_tree = in_tree.CopyTree("")
    for n in kept_ntuples:
      copied_ntuples.append( n.CopyTree("") )
    out_file.Write()
      
    
if __name__ == '__main__':
  steer()
