#include <iostream>
#include "include/MTSampleHandler.h"
#include "include/RatioPlot.h"
#include "include/ROC.h"
#include "include/MultiPlot.h"
#include "include/Utility.h"
#include "TH1.h"
#include "TTree.h"
#include "TCanvas.h"
#include "TPad.h"
#include "TFile.h"
#include "TStyle.h"

using namespace std;

// Where the magic happens
void RunAnalysis( Bool_t test = false ) {

  cout << "RunAnalysis(): Begin." << endl;
  gStyle->SetOptStat(0);
  gStyle->SetLegendBorderSize(0);
}


// Entry point for executable. Parse command line input and call RunAnalysis
int main( int argc, const char* argv[] ) {
  
  if ( argc != 1 && argc != 2 ) {
    std::cerr << "Usage: Run [test]" << std::endl;
    return 1;
  }
  
  Bool_t test = false;
  if ( argc == 2 ) {
    if ( strcmp(argv[1], "true" ) || strcmp( argv[1], "test" ) )
      test = true;
  }
  
  RunAnalysis( test );
  
  return 0;
}

