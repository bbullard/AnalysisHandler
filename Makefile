ROOTCFLAGS    = $(shell $(ROOTSYS)/bin/root-config --cflags)
ROOTGLIBS     = $(shell $(ROOTSYS)/bin/root-config --glibs)
ROOTVERSION   = $(shell $(ROOTSYS)/bin/root-config --version)
CMAKEVERSION  = $(shell cmake --version)

CXX            = g++

CXXFLAGS       = -std=c++17 -fPIC -Wall -Wextra -pthread -O3 -g
CXXFLAGS       += $(filter-out -stdlib=libc++ -std=c++1z -pthread , $(ROOTCFLAGS))

GLIBS          = $(filter-out -stdlib=libc++ -std=c++1z -pthread , $(ROOTGLIBS))

INCLUDEDIR       = ./include/
SRCDIR           = ./
CXX	         	+= -I$(INCLUDEDIR) -I.
OUTOBJ	         = ./obj/

CC_FILES := $(wildcard *.cxx)
H_FILES := $(wildcard include/*.h)
OBJ_FILES := $(addprefix $(OUTOBJ),$(notdir $(CC_FILES:.C=.o)))

UNAME_S := $(shell uname -s)
SHELL := /bin/bash

all: rootver cmakever Run
ifeq ($(UNAME_S),Darwin)
	    install_name_tool -change libNSWMapperLib.dylib dep/NSWMapper/build/libNSWMapperLib.dylib Run
endif


# We want ROOT 6+
rootver:
ifeq  ($(findstring 6.,$(ROOTVERSION)),)
	$(error $(NL) Your ROOT version is $(ROOTVERSION). $(NL) Please use setup.sh $(NL))
endif


# We want cmake 3+
cmakever:
ifeq  ($(findstring 3.,$(CMAKEVERSION)),)
	$(error $(NL) Your cmake version is $(CMAKEVERSION). $(NL) Please use setup.sh $(NL))
endif


Run :  $(SRCDIR)Run.cxx $(H_FILES)
	$(CXX) $(CXXFLAGS) \
	-o Run $(GLIBS) $ $<
#	touch Run


clean:
	rm -f Run
